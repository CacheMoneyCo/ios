**Frontdoor by Cache Money Co**

Nathan Amarandos, Eric Van Lare, Michael Hao

*Copyright 2018*

---

## Install Dependencies
Install CocoaPods
```
gem install cocoapods
```
Create Podfile and Add Pod for AFNetworking
```
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '8.0'
target 'TargetName' do
pod 'AFNetworking', '~> 3.0'
end
```
Build the Podfile
```
pod install
```

