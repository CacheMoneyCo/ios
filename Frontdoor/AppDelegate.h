//
//  AppDelegate.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/11/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

