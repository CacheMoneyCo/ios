//
//  CommentButton.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/15/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "CommentButton.h"

@implementation CommentButton
@synthesize commentData, cell;

- (void)setupButton:(NSDictionary*)data{
    commentData = data;
    [self setTitle:[self commentTitle:data] forState:UIControlStateNormal];
}

- (NSArray*)getData{
    return (NSArray*)commentData;
}

- (UITableViewCell*)getCell{
    return (UITableViewCell*)cell;
}

- (NSString*)commentTitle:(NSDictionary*)dict{
    NSString *str = [NSString stringWithFormat:@"Add Comment"];
    NSArray *com = (NSArray*)[dict objectForKey:@"comments"];
    NSNumber *num = [[NSNumber alloc] initWithUnsignedLong:[com count]];
    if ([num integerValue] == 1){
        str = [[NSString alloc] initWithFormat:@"%li Comment", (long)[num integerValue]];
    }
    else if ([num integerValue] > 0){
        str = [[NSString alloc] initWithFormat:@"%li Comments", (long)[num integerValue]];
    }
    return str;
}


@end
