//
//  CommentButton.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/15/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentButton : UIButton

- (NSArray*)getData;
- (UITableViewCell*)getCell;

- (void)setupButton:(NSDictionary*)data;

@property (nonatomic, retain) id commentData;
@property (nonatomic, retain) UITableViewCell *cell;
@end
