//
//  LikeButton.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "LikeButton.h"

@implementation LikeButton

- (id)init{
    self = [super init];
    if (self){
        numLikes = 0;
    }
    return self;
}

- (void)setNumberofLikes:(NSNumber*)num{
    switch ([num integerValue]) {
        case 0:
            [self setImage:[[UIImage imageNamed:@"emptyHeart"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            [self setTitle:@" " forState:UIControlStateNormal];
            [self setTintColor:[UIColor lightGrayColor]];
            break;
        default:
            [self setImage:[[UIImage imageNamed:@"fullHeart"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            [self setTintColor:[UIColor colorWithRed:0.85 green:0.56 blue:0.50 alpha:1.0]];
            [self setTitle:[NSString stringWithFormat:@"%li", [num integerValue]] forState:UIControlStateNormal];
            [self setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            break;
    }
}

@end
