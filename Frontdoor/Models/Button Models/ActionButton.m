//
//  ActionButton.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "ActionButton.h"

@implementation ActionButton

- (void)setSelected:(id)sender{
    ActionButton *button = (ActionButton*)sender;
    [button setBackgroundColor:[UIColor colorWithRed:0.27 green:0.63 blue:0.97 alpha:1.00]];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (void)setDeselected:(id)sender{
    ActionButton *button = (ActionButton*)sender;
    [button setBackgroundColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.00]];
    [button setTitleColor:[UIColor colorWithRed:0.27 green:0.63 blue:0.97 alpha:1.00] forState:UIControlStateNormal];
}

@end
