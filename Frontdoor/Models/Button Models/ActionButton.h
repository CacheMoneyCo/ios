//
//  ActionButton.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionButton : UIButton

//Voting Button Properties/Methods
@property (nonatomic, retain)NSArray *buttonInfo;
@property (nonatomic) BOOL *isSelected;
- (void)setSelected:(id)sender;
- (void)setDeselected:(id)sender;

@end
