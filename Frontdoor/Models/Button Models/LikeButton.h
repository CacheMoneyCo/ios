//
//  LikeButton.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikeButton : UIButton
{
    int numLikes;
}
- (id)init;
- (void)setNumberofLikes:(NSNumber*)num;
@end
