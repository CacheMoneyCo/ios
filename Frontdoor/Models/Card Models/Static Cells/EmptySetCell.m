//
//  EmptySetCell.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "EmptySetCell.h"

@implementation EmptySetCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        UILabel *empty = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [empty setText:@"No cards retrieved, swipe up to reload."];
        [empty setTextAlignment:NSTextAlignmentCenter];
        [self.contentView addSubview:empty];
        [empty setCenter:self.contentView.center];
    }
    
    return self;
}

@end
