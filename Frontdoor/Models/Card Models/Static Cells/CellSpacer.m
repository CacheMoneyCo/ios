//
//  CellSpacer.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/12/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "CellSpacer.h"

@implementation CellSpacer

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self.contentView setBackgroundColor:[UIColor colorWithRed:0.94 green:0.94 blue:0.96 alpha:1.00]];
    }
    
    return self;
}

@end
