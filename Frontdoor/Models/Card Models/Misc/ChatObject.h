//
//  ChatObject.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/5/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatObject : NSObject
- (id)initWithProperties:(NSDictionary*)arr;
@property (nonatomic, retain)NSNumber *lease;
@property (nonatomic, retain)NSNumber *likes;
@property (nonatomic, retain)NSString *poster;
@property (nonatomic, retain)NSString *message;
@property (nonatomic, retain)NSDate *date;
@end
