//
//  ChatObject.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/5/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "ChatObject.h"

@implementation ChatObject
- (id)initWithProperties:(NSDictionary*)info{
    self = [super init];
    if (self){
        _lease = [info objectForKey:@"lease"];
        _likes = [info objectForKey:@"likes"];
        _message = [info objectForKey:@"message"];
        _poster = [info objectForKey:@"poster"];
        _date = [self parseDate:[info objectForKey:@"time"]];
    }
    return self;
}

- (NSDate*)parseDate:(id)date{
    if ([date isKindOfClass:[NSDate class]]){
        return date;
    }
    else{
        NSString *str = (NSString*)date;
        NSString *dateStr = [[str componentsSeparatedByString:@"T"] objectAtIndex:0];
        NSString *timeStr = [[[[str componentsSeparatedByString:@"T"] objectAtIndex:1] componentsSeparatedByString:@"."] objectAtIndex:0];
        NSString *comboStr = [[dateStr stringByAppendingString:@"-"] stringByAppendingString:timeStr];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd-HH:mm:ss"];
        NSDate *date = [dateFormatter dateFromString:comboStr];
        return date;
    }
}
@end
