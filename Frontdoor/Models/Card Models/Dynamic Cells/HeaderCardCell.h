//
//  HeaderCardCell.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@protocol PresentNewPostDelegate;

@interface HeaderCardCell : UITableViewCell

@property (nonatomic, retain)IBOutlet UIButton *eventButton;
@property (nonatomic, retain)IBOutlet UIButton *taskButton;
@property (nonatomic, retain)IBOutlet UIButton *voteButton;
@property (nonatomic, retain)IBOutlet UIButton *paymentButton;

@property (nonatomic, retain)NSNumber *cellHeight;

@property (nonatomic, retain) id<PresentNewPostDelegate> delegate;

//For finding the cell later
@property (nonatomic, retain) NSString *cellID;
@property (nonatomic, retain) NSDictionary *dict;

- (id) initWithHeight:(NSNumber*)height;

- (IBAction)addAnnoucement:(id)sender;
- (IBAction)addEvent:(id)sender;
- (IBAction)addTask:(id)sender;
- (IBAction)addVote:(id)sender;
- (IBAction)addPayment:(id)sender;
- (void)setDelegate:(id<PresentNewPostDelegate>)delegate;
@end

@protocol PresentNewPostDelegate <NSObject>
-(void)presentNewPost:(CardType)type;
@end
