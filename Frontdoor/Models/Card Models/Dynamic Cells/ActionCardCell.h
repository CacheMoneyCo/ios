//
//  ActionCardCell.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActionButton.h"
#import "JSONParser.h"

@class JSONParser;

@interface ActionCardCell : UITableViewCell
{
    JSONParser *jH;
}

@property (nonatomic, retain)IBOutlet UILabel *titleLabel;
@property (nonatomic, retain)IBOutlet UILabel *dateLabel;
@property (nonatomic, retain)IBOutlet UILabel *secondaryLabel;
@property (nonatomic, retain)IBOutlet UILabel *descriptionLabel;
@property (nonatomic, retain)IBOutlet ActionButton *actionButton;

//For finding the cell later
@property (strong, nonatomic) NSString *cellID;
@property (strong, nonatomic) NSDictionary *dict;

- (id) initWithPaymentCard:(NSArray*)array;
- (id) initWithSetupCard:(NSArray*)array;
- (id) initWithSubleaseRequestCard:(NSArray*)array;
@end
