//
//  CommentCardCell.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/31/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentCardCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *announceLabel;
@property (strong, nonatomic) IBOutlet UILabel *detailLabel;

@end
