//
//  BasicCardCell.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/14/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LikeButton.h"

@interface BasicCardCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextView *announceLabel;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet LikeButton *likeButton;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;

@property (strong, nonatomic) IBOutlet NSNumber *cellHeight;

//For finding the cell later
@property (strong, nonatomic) NSString *cellID;
@property (strong, nonatomic) NSDictionary *dict;

//Custructors
- (id) initWithAnnouncement:(NSArray*)info;
- (id) initWithTask:(NSArray*)info;

//Helper Methods
- (NSNumber*)getCellHeight;

@end
