//
//  ActionCardCell.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "ActionCardCell.h"

@implementation ActionCardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id) initWithPaymentCard:(NSArray*)array{//0.owner, 1.amount(NSnumber), 2.date(NSDate), 3.height (NSNumber), . title
    self = [super init];
    if (self) {
        jH = [[JSONParser alloc]init];
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"ActionCardCellView" owner:self options:nil];
        NSString *amount = [NSString stringWithFormat:@"Pay\n$%.2f", [[array objectAtIndex:1] floatValue]];
        NSString *dateString = [self stringFromDate:[array objectAtIndex:2]];
        ActionCardCell *cell = (ActionCardCell*)[nibViews objectAtIndex:0];
        [cell.titleLabel setText:[array objectAtIndex:0]];
        [cell.secondaryLabel setText:[array objectAtIndex:4]];
        [cell.dateLabel setText:dateString];
        [cell.descriptionLabel setText:@"Pay with Stripe"];
        [cell.actionButton setTitle:amount forState:UIControlStateNormal];
        cell.actionButton.titleLabel.numberOfLines = 2;
        [cell.actionButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [cell.actionButton addTarget:self action:@selector(handlePayment:) forControlEvents:UIControlEventTouchUpInside];
        [cell setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [[array objectAtIndex:3] floatValue])];
        [self addSubview:cell];
    }
    return self;
}

- (id) initWithSetupCard:(NSArray*)array{//0.title(NSString), 1.date(NSDate), 2.height(NSNumber)
    self = [super init];
    if (self) {
        jH = [[JSONParser alloc]init];
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"ActionCardCellView" owner:self options:nil];
        NSString *dateString = [self stringFromDate:[array objectAtIndex:1]];
        ActionCardCell *cell = (ActionCardCell*)[nibViews objectAtIndex:0];
        [cell.titleLabel setText:[array objectAtIndex:0]];
        [cell.secondaryLabel setText:dateString];
        [cell.descriptionLabel setText:@""];
        [cell.actionButton setTitle:@"Setup" forState:UIControlStateNormal];
        [cell.actionButton addTarget:self action:@selector(handleSetup:) forControlEvents:UIControlEventTouchUpInside];
        [cell setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [[array objectAtIndex:2] floatValue])];
        [self addSubview:cell];
    }
    return self;
}

- (id) initWithSubleaseRequestCard:(NSArray*)array{//0.title(NSString), 1.date(NSDate), 2.message(NSString), 3.subleaserName(NSString),  4.height(NSNumber)
    self = [super init];
    if (self) {
        jH = [[JSONParser alloc]init];
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"ActionCardCellView" owner:self options:nil];
        NSString *dateString = [self stringFromDate:[array objectAtIndex:1]];
        ActionCardCell *cell = (ActionCardCell*)[nibViews objectAtIndex:0];
        [cell.titleLabel setText:[array objectAtIndex:3]];
        [cell.secondaryLabel setText:dateString];
        [cell.descriptionLabel setText:[array objectAtIndex:0]];
        [cell.actionButton setTitle:@"View\nProfile" forState:UIControlStateNormal];
        cell.actionButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.actionButton.titleLabel.numberOfLines = 2;
        cell.actionButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [cell.actionButton addTarget:self action:@selector(handleSubleaseRequest:) forControlEvents:UIControlEventTouchUpInside];
        [cell setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [[array objectAtIndex:4] floatValue])];
        [self addSubview:cell];
    }
    return self;
}

- (NSString*)stringFromDate:(NSDate*)date{
    NSString *str = [[NSString alloc] init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init]; 
    [df setDateFormat:@"MMM d, yyyy"];
    str = [df stringFromDate:date];
    return str;
}

#pragma mark -
#pragma mark Function Defintions

- (void)handlePayment:(id)sender{
    //tell the json the payment has been made once we can confirm that
}

- (void)handleSetup:(id)sender{
    
}

- (void)handleSubleaseRequest:(id)sender{
    //get the json for the subleaser account, or however you wanna do this 
}

- (NSInvocation*)getFunctionForType:(NSString*)type{
    NSInvocation* func;
    return func;
}


@end
