//
//  EventCardCell.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "EventCardCell.h"

@implementation EventCardCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id) initWithEvent:(NSArray*)info{
    self = [super init];
    if (self){
        //title, height, postedBy, date, message
        NSString *title = [info objectAtIndex:0];
        NSNumber *height = [info objectAtIndex:1];
        NSString *postedBy = [info objectAtIndex:2];
        NSDate *date = [info objectAtIndex:3];
        //NSString *message = [info objectAtIndex:4];
        
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"EventCardCellView" owner:self options:nil];
        EventCardCell *cell = (EventCardCell *)[nibViews objectAtIndex:0];
        [cell.titleLabel setText:title];
        [cell.ownerLabel setText:[NSString stringWithFormat:@"By %@", postedBy]];
        [cell.descriptionLabel setText:@"All Day"];
        [cell.dayLabel setText:[self dayStringFromDate:date]];
        [cell.dateIcon setImage: [UIImage imageNamed:[NSString stringWithFormat:@"cal_%@", [self dateStringFromDate:date]] ]];
        [cell.timeLabel setText:[self timeStringFromDate:date]];
        [cell setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [height floatValue])];
        [self.contentView addSubview:cell];
    }
    return self;
}

- (NSString*)dateStringFromDate:(NSDate*)date{
    NSString *str = [[NSString alloc] init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"d"];
    str = [df stringFromDate:date];
    return str;
}

- (NSString*)dayStringFromDate:(NSDate*)date{
    NSString *str = [[NSString alloc] init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MMM"];
    str = [df stringFromDate:date];
    return str;
}

- (NSString*)timeStringFromDate:(NSDate*)date{
    NSString *str = [[NSString alloc] init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:mm"];
    str = [df stringFromDate:date];
    return str;
}

@end
