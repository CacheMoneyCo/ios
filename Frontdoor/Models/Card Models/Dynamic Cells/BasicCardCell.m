//
//  BasicCardCell.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/12/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "BasicCardCell.h"

@implementation BasicCardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    //Initialization code
}

- (id) initWithAnnouncement:(NSArray*)info{
    //announcement(str), height, postedBy, date, likes
    self = [super init];
    if (self){
        NSString *str = [info objectAtIndex:0];
        _cellHeight = [info objectAtIndex:1];
        NSString *postedBy = [info objectAtIndex:2];
        NSDate *date = [info objectAtIndex:3];
        //NSArray *likes = [info objectAtIndex:4]; legacy
        
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"BasicCardCellNib" owner:self options:nil];
        BasicCardCell *cell = (BasicCardCell*)[nibViews objectAtIndex:0];
        [cell.announceLabel setText:str];
        [cell.announceLabel setTextContainerInset:UIEdgeInsetsZero];
        cell.announceLabel.textContainer.lineFragmentPadding = 0; // to remove left padding
        _cellHeight = [self calculateCellHeight:str];
        [cell.titleLabel setText:postedBy];
        //[cell.likeButton setData:likes]; //legacy
        [cell.dateLabel setText:[self stringFromDate:date]];
        //[cell.likeButton addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [_cellHeight floatValue])];
        [self.contentView addSubview:cell];
    }
    return self;
}

- (id) initWithTask:(NSArray*)info{
    self = [super init];
    if (self){
        //title, height, postedBy, date
        NSString *str = [info objectAtIndex:0];
        _cellHeight = [info objectAtIndex:1];
        NSString *postedBy = [info objectAtIndex:2];
        NSDate *date = [info objectAtIndex:3];
        
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"BasicCardCellNib" owner:self options:nil];
        BasicCardCell *cell = (BasicCardCell*)[nibViews objectAtIndex:0];
        [cell.announceLabel setText:str];
        [cell.announceLabel setTextContainerInset:UIEdgeInsetsZero];
        cell.announceLabel.textContainer.lineFragmentPadding = 0; // to remove left padding
        _cellHeight = [self calculateCellHeight:str];
        [cell.titleLabel setText:postedBy];
        [cell.dateLabel setText:[self stringFromDate:date]];
        //[cell.likeButton addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [_cellHeight floatValue])];
        [self.contentView addSubview:cell];
    }
    return self;
}

- (NSString*)stringFromDate:(NSDate*)date{
    NSString *str = [[NSString alloc] init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MMM d, yyyy"];
    str = [df stringFromDate:date];
    return str;
}

- (NSNumber*)getCellHeight{
    return _cellHeight;
}

- (NSNumber*)calculateCellHeight:(NSString*)str{
    NSNumber *lines = [NSNumber numberWithInteger:[str length]/50];
    NSNumber *height = [NSNumber numberWithInteger:71+(20*[lines integerValue])];
    return height;
}

- (void)likeAction:(id)sender{
    //LikeButton* lb = (LikeButton*)sender;
    //[lb toggleButton];
}

@end


