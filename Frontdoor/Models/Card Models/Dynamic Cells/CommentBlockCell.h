//
//  CommentBlockCell.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/1/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentButton.h"
#import "LikeButton.h"

@interface CommentBlockCell : UITableViewCell
{
    NSDictionary* dictPass;
    NSNumber *heightPass;
}
@property (nonatomic, retain) IBOutlet CommentButton *cButton;
@property (nonatomic, retain) IBOutlet LikeButton *likeButton;

//For finding the cell later
@property (strong, nonatomic) NSString *cellID;
@property (strong, nonatomic) NSDictionary *dict;

@end
