//
//  EventCardCell.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventCardCell : UITableViewCell

@property (nonatomic, retain)IBOutlet UILabel *dayLabel;
@property (nonatomic, retain)IBOutlet UILabel *dateLabel;
@property (nonatomic, retain)IBOutlet UIImageView *dateIcon;
@property (nonatomic, retain)IBOutlet UILabel *titleLabel;
@property (nonatomic, retain)IBOutlet UILabel *timeLabel;
@property (nonatomic, retain)IBOutlet UILabel *ownerLabel;
@property (nonatomic, retain)IBOutlet UILabel *descriptionLabel;

//For finding the cell later
@property (strong, nonatomic) NSString *cellID;
@property (strong, nonatomic) NSDictionary *dict;

- (id) initWithEvent:(NSArray*)info;
@end
