//
//  HeaderCardCell.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "HeaderCardCell.h"

@implementation HeaderCardCell
@synthesize delegate;

- (id) initWithHeight:(NSNumber*)height{
    self = [super init];
    if (self) {
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"HeaderCardCellView" owner:self options:nil];
        self = (HeaderCardCell*)[nibViews objectAtIndex:0];
        [self setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [height floatValue])];
        [self setCellHeight:height];
    }
    return self;
}

#pragma mark -
#pragma mark Add Things
- (IBAction)addAnnoucement:(id)sender{
    if ([delegate respondsToSelector:@selector(presentNewPost:)]) {
        [delegate presentNewPost:kCardAnnouncement];
    }
}

- (IBAction)addEvent:(id)sender{
    if ([delegate respondsToSelector:@selector(presentNewPost:)]) {
        [delegate presentNewPost:kCardEvent];
    }
}

- (IBAction)addTask:(id)sender{
    if ([delegate respondsToSelector:@selector(presentNewPost:)]) {
        [delegate presentNewPost:kCardTask];
    }
}

- (IBAction)addVote:(id)sender{
    if ([delegate respondsToSelector:@selector(presentNewPost:)]) {
        [delegate presentNewPost:kCardVote];
    }
}

- (IBAction)addPayment:(id)sender{
    if ([delegate respondsToSelector:@selector(presentNewPost:)]) {
        [delegate presentNewPost:kCardPayment];
    }
}

@end
