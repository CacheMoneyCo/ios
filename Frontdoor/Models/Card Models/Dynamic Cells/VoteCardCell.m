//
//  VoteCardCell.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "VoteCardCell.h"

@implementation VoteCardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id) initWithVote:(NSArray*)info{
    self = [super init];
    if (self){
        //title, height, date, yesvotes, novotes, (owner)
        NSString *title = [info objectAtIndex:0];
        NSNumber *height = [info objectAtIndex:1];
        NSDate *date = [info objectAtIndex:2];
        yesVotes = [info objectAtIndex:3];
        noVotes = [info objectAtIndex:4];
        NSString *postedBy = [info objectAtIndex:5];
        
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"VoteCardCellView" owner:self options:nil];
        VoteCardCell *cell = (VoteCardCell *)[nibViews objectAtIndex:0];
        [cell.ownerLabel setText:postedBy];
        [cell.questionLabel setText:title];
        [cell.questionLabel setTextContainerInset:UIEdgeInsetsZero];
        cell.questionLabel.textContainer.lineFragmentPadding = 0;
        [self updateSegControl];
        [cell.segControl setTitle:[NSString stringWithFormat:@"Yes: %li", [yesVotes count]] forSegmentAtIndex:0];
        [cell.segControl setTitle:[NSString stringWithFormat:@"No: %li", [noVotes count]] forSegmentAtIndex:1];
        [cell.dateLabel setText:[self stringFromDate:date]];
        [cell setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [height floatValue])];
        //jsonHelp = [[JSONParser alloc] init];
        [self.contentView addSubview:cell];
    }
    return self;
}

- (NSString*)stringFromDate:(NSDate*)date{
    NSString *str = [[NSString alloc] init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MMM d, yyyy"];
    str = [df stringFromDate:date];
    return str;
}

#pragma mark -
#pragma mark Segmented Control and JSON Handling

- (IBAction)segDidChange:(id)sender{
    //update json with helper class
    //update yesvotes and novotes
    [self updateSegControl];
}

- (void)updateSegControl{//Reads didVoteYes from method and then formats
    NSString *vote = [self didVoteYes];
    if ([vote isEqualToString:@"yes"]){
        [_segControl setSelectedSegmentIndex:0];
        [_segControl setTintColor:[UIColor greenColor]];
    }
    else if ([vote isEqualToString:@"no"]){
        [_segControl setSelectedSegmentIndex:1];
        [_segControl setTintColor:[UIColor redColor]];
    }
    [_segControl setTitle:[NSString stringWithFormat:@"Yes: %li", [yesVotes count]] forSegmentAtIndex:0];
    [_segControl setTitle:[NSString stringWithFormat:@"No: %li", [noVotes count]] forSegmentAtIndex:1];
}

- (NSString*)didVoteYes{
    NSString *str = [[NSString alloc] initWithFormat:@"und"];
    #warning Check to see if youve liked the buttons already or nah
    return str;
}

@end
