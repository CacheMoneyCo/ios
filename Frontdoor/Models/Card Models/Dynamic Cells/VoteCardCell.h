//
//  VoteCardCell.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSONParser.h"

@interface VoteCardCell : UITableViewCell
{
    NSMutableArray *yesVotes;
    NSMutableArray *noVotes;
}
@property (nonatomic, retain)IBOutlet UITextView *questionLabel;
@property (nonatomic, retain)IBOutlet UILabel *ownerLabel;
@property (nonatomic, retain)IBOutlet UILabel *dateLabel;
@property (nonatomic, retain)IBOutlet UISegmentedControl *segControl;

//For finding the cell later
@property (strong, nonatomic) NSString *cellID;
@property (strong, nonatomic) NSDictionary *dict;

- (id) initWithVote:(NSArray*)info;
- (IBAction)segDidChange:(id)sender;

@end

