//
//  ChatViewController.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/15/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSONParser.h"
#import "BackendConnector.h"
#import "ChatObject.h"
#import "HouseMembers.h"

@interface ChatViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSURLSessionDelegate>
{
    JSONParser *jsonHandler;
    __block NSMutableArray *chatDataSource;
    UIRefreshControl *refControl;
}
@property (nonatomic, retain)IBOutlet UITextField* textField;
@property (nonatomic, retain)IBOutlet UITableView* tableView;
@property (nonatomic, retain)IBOutlet NSLayoutConstraint *bottomContstraint;

@end
