//
//  ChatViewController.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/15/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "ChatViewController.h"
#import "BackendConnector.h"

#define CHAT_URL [NSString stringWithFormat:@"%@chats/", baseFrontdoorURL]
#define CHAT_CREATE_URL [NSString stringWithFormat:@"%@chat/create/", baseFrontdoorURL]

@interface ChatViewController ()

@end

@implementation ChatViewController
@synthesize textField, tableView, bottomContstraint;

- (void)viewDidLoad {
    [super viewDidLoad];
    jsonHandler = [[JSONParser alloc] init];
    chatDataSource = [[NSMutableArray alloc] init];
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser
                                                         password:baseFrontdoorCredPass
                                                      persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred
                                                                  andURL:CHAT_URL
                                                             andDelegate:self];
    [bc requestData];

    
    [self setTitle:@"House Chat"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWasShown:)
     name:UIKeyboardWillShowNotification
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillBeHidden:)
     name:UIKeyboardWillHideNotification
     object:nil];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.transform = CGAffineTransformMakeRotation(-M_PI);
    [self.tableView setShowsHorizontalScrollIndicator:NO];
    [self.tableView setShowsVerticalScrollIndicator:NO];
    refControl = [[UIRefreshControl alloc] init];
    [refControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [tableView setRefreshControl:refControl];
    [self updateTableView];
}

- (void)updateTableView{
    [tableView reloadData];
}
- (void)leftMenu:(id)sender{
    NSLog(@"Touched");
}

- (void)refresh:(id)sender{
    chatDataSource = [[NSMutableArray alloc]init];
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser password:baseFrontdoorCredPass persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred andURL:CHAT_URL andDelegate:self];
    [bc requestData];
    [self.tableView reloadData];
    [tableView.refreshControl endRefreshing];
}

- (void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]
     removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Keyboard Functions
- (void)keyboardWasShown:(NSNotification*)notification{
    [self adjustHeight:true withNotification:notification];
}

- (void)keyboardWillBeHidden:(NSNotification*)notification{
    [self adjustHeight:false withNotification:notification];
}

- (void)adjustHeight:(BOOL)show withNotification:(NSNotification*)notification{
    NSDictionary* info = [notification userInfo];
    CGRect frame = [[info objectForKey:@"UIKeyboardFrameBeginUserInfoKey"]CGRectValue];
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *change = [NSNumber numberWithInteger:frame.size.height *  (show ? 1 : -1)];
    [UIView animateWithDuration:[duration doubleValue] animations:^(void){
        CGFloat update = bottomContstraint.constant - [change doubleValue];
        [bottomContstraint setConstant:update];
        [self.view layoutIfNeeded];
    }];
}

-(void)dismissKeyboard{
    [self adjustHeight:false withNotification:[NSNotification notificationWithName:UIKeyboardWillHideNotification object:self]];
    [textField resignFirstResponder];
    [self.view endEditing:TRUE];
}

- (IBAction)postChat:(id)sender{
    UITextField *chat = (UITextField*)sender;
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser
                                                         password:baseFrontdoorCredPass
                                                      persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred
                                                                  andURL:CHAT_CREATE_URL
                                                             andDelegate:self];
    NSArray* params = @[ @{ @"name": @"message", @"value": chat.text }];
    [bc postWithParams:params];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSDictionary *member = [def objectForKey:@"member_self"];
    //NSString *name = [NSString stringWithFormat:@"%@ %@", [member objectForKey:@"first_name"], [member objectForKey:@"last_name"]];
    
    NSMutableDictionary* info = [[NSMutableDictionary alloc]init];
    [info setValue:chat.text forKey:@"message"];
    [info setValue:[member objectForKey:@"username"] forKey:@"poster"];
    [info setValue:[NSDate dateWithTimeIntervalSinceNow:0]  forKey:@"time"];
    [info setValue:[NSNumber numberWithInt:0]  forKey:@"likes"];
    [info setValue:[NSNumber numberWithInt:0]  forKey:@"lease"];
    
    ChatObject *c = [[ChatObject alloc] initWithProperties:info];
    [chatDataSource insertObject:c atIndex:0];
    [textField setText:@""];
    [self updateTableView];
}

#pragma mark - Table View Setup
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [chatDataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"textChat"];
    UILabel* name = [cell viewWithTag:100];
    UILabel* message = [cell viewWithTag:101];
    LikeButton *like = [cell viewWithTag:200];
    
    ChatObject *c = [chatDataSource objectAtIndex:indexPath.row];
    [name setText:c.poster];
    if ([HouseMembers isUser:c.poster]){
        [cell.contentView setBackgroundColor:[UIColor colorWithRed:0.33 green:0.70 blue:0.71 alpha:.15]];
    }
    [like setNumberofLikes:c.likes];
    [message setText:c.message];
    
    
    cell.transform = CGAffineTransformMakeRotation(M_PI);
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
#warning set the height to 50 for date cells, and then to arbitrary height depending on the length of post
    return 50;
}

- (void)updateTableContentInset {
    NSInteger numRows = [self tableView:self.tableView numberOfRowsInSection:0];
    CGFloat contentInsetTop = self.tableView.bounds.size.height;
    for (NSInteger i = 0; i < numRows; i++) {
        contentInsetTop -= [self tableView:self.tableView heightForRowAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
        if (contentInsetTop <= 0) {
            contentInsetTop = 0;
            break;
        }
    }
    self.tableView.contentInset = UIEdgeInsetsMake(contentInsetTop, 0, 0, 0);
}

#pragma mark - NSURLSessionDelegate

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
   didReceiveData:(NSData *)data {
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    if (dictionary){
        dispatch_async(dispatch_get_main_queue(), ^{
            chatDataSource = [jsonHandler parseChatDict:dictionary];
            [self.tableView reloadData];
        });
    }
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler{
    if ([challenge previousFailureCount] == 0) {
        NSLog(@"received and handle authentication challenge");
        NSURLCredential *newCredential = [NSURLCredential credentialWithUser:baseFrontdoorCredUser
                                                                    password:baseFrontdoorCredPass
                                                                 persistence:NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
    }
    else {
        NSLog(@"previous authentication failure");
    }
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error{
    NSLog(@"%@", error);
}



@end
