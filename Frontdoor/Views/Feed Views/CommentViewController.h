//
//  CommentViewController.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/31/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentCardCell.h"
#include "CardHandler.h"

@interface CommentViewController : UITableViewController 
{
    NSNumber *origCellHeight;
}
- (void)setCommentData:(NSDictionary*)data;
@property (nonatomic, retain) NSDictionary *commentData;
@property (nonatomic, retain)NSMutableArray *comments;
@end
