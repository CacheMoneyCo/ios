//
//  CommentViewController.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/31/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "CommentViewController.h"

@interface CommentViewController ()

@end

@implementation CommentViewController
@synthesize comments;

- (void)viewDidLoad {
    [super viewDidLoad];
    comments = [NSMutableArray arrayWithArray:[_commentData objectForKey:@"comments"]];
    if ([comments count] == 0)
        comments = [NSMutableArray arrayWithObjects:@"", nil];
    else
        [comments insertObject:[comments objectAtIndex:0] atIndex:0];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.allowsSelection = FALSE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [comments count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0){
        CardHandler *card = [[CardHandler alloc] initWithJSONDictionary:_commentData];
        origCellHeight = (NSNumber*)[card getPropertyByName:@"cellHeight"];
        return (UITableViewCell*)[card getPropertyByName:@"cell"];
    }
    CommentCardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"commentCell"];
    if (!cell){
        [self.tableView registerNib:[UINib nibWithNibName:@"CommentCardCellView" bundle:nil] forCellReuseIdentifier:@"commentCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"commentCell"];
    }
    [cell.announceLabel setText:[(NSDictionary*)[comments objectAtIndex:indexPath.row] objectForKey:@"postedby"]];
    [cell.detailLabel setText:[(NSDictionary*)[comments objectAtIndex:indexPath.row] objectForKey:@"message"]];
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0){
        return [origCellHeight floatValue];
    }
    return 44;
}

#pragma mark -
#pragma mark Setting Properties

- (void)setCommentData:(NSDictionary*)data{
    if (data){
        _commentData = [NSDictionary dictionaryWithDictionary:data];
    }
}

@end
