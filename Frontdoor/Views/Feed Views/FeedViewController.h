//
//  FeedViewController.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/11/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardHandler.h"
#import "CommentButton.h"
#import "CommentBlockCell.h"
#import "HeaderCardCell.h"
#import "CommentViewController.h"

#import "PostEventViewController.h"
#import "PostTaskViewController.h"
#import "PostVoteViewController.h"
#import "PostAnnouncementViewController.h"
#import "PostPaymentViewController.h"

#import "JSONParser.h"
#import "SlideRightSegue.h"

#import "BackendConnector.h"

#import "HouseMembers.h"
#import "HouseRooms.h"

@class JSONParser;

@interface FeedViewController : UITableViewController <UISearchBarDelegate, NSURLSessionDelegate, PresentNewPostDelegate>
{
    //TableViewVariables
    NSArray *cards;
    NSMutableArray *cardHandlers;
    NSDictionary *lastCellDict;//for comments
    //Search Variables
    UISearchBar *searchField;
    NSMutableArray *filteredCards;
    BOOL isSearching;
    UIRefreshControl *refControl;
    JSONParser *jsonHelp;
}

@end
