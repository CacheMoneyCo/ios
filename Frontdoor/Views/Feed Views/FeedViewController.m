//
//  FeedViewController.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/11/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "FeedViewController.h"

#define CARD_URL [NSString stringWithFormat:@"%@cards/", baseFrontdoorURL]

@interface FeedViewController ()

@end

@implementation FeedViewController

- (void)viewDidAppear:(BOOL)animated{
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser
                                                         password:baseFrontdoorCredPass
                                                      persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred
                                                                  andURL:CARD_URL
                                                             andDelegate:self];
    [bc requestData];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    cardHandlers = [[NSMutableArray alloc] init];
    filteredCards = [[NSMutableArray alloc] init];
    refControl = [[UIRefreshControl alloc] init];
    [refControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
    [self setRefreshControl:refControl];
    [self viewDetailing];
    
    [[[HouseMembers alloc] init] getMembers];
    [[[HouseRooms alloc] init] getRooms];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - View Creation Stuff

- (void)viewDetailing{
    //Navigation Bar Styling
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.34 green:0.65 blue:0.35 alpha:1.00];
    
    //Search Bar init
    searchField = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height)];
    //[searchField setImage:[UIImage imageNamed:@"searchImage"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    [searchField setTintColor:[UIColor whiteColor]];
    UITextField *textField = [searchField valueForKey:@"searchField"];
    [textField setBackgroundColor:[UIColor colorWithRed:0.31 green:0.53 blue:0.33 alpha:1.00]];
    [textField setTextColor: [UIColor whiteColor]];
    
    UIImageView *leftImageView = (UIImageView *)textField.leftView;
    leftImageView.image = [leftImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    leftImageView.tintColor = [UIColor whiteColor];
    
    UIButton *clearButton = [textField valueForKey:@"clearButton"];
    [clearButton setImage:[clearButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    clearButton.tintColor = [UIColor whiteColor];
    
    
    [searchField setDelegate:self];
    self.navigationItem.titleView = searchField;
    
    //TableView Style Editing
    [self.tableView setShowsHorizontalScrollIndicator:NO];
    [self.tableView setShowsVerticalScrollIndicator:NO];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.allowsSelection = FALSE;
}

#pragma mark - Search Bar Setup

- (void) searchBarSearchButtonClicked:(UISearchBar*) theSearchBar
{
    //isSearching = NO;
    [self.tableView reloadData];
    [theSearchBar resignFirstResponder];
    [theSearchBar setShowsCancelButton:NO animated:YES];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    isSearching = NO;
    [self.tableView reloadData];
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [filteredCards removeAllObjects];
    if([searchText length] != 0) {
        isSearching = YES;
        [self searchTableList];
    }
    else {
        isSearching = NO;
        [self.tableView reloadData];
    }
}

- (void)searchTableList {//Search by Title for now
    NSString *searchString = searchField.text;
    for (CardHandler *card in cards) {
        NSString *title = (NSString*)[card getPropertyByName:@"title"];
        if (title != NULL){
            NSComparisonResult result = [title compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:[title rangeOfString:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)]];
            if (result == NSOrderedSame) {
                [filteredCards addObject:card];
                [filteredCards addObject:[[CardHandler alloc] initWithSpacer]];
            }
        }
    }
}

#pragma mark - Table View Setup
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isSearching)
        return [filteredCards count];
    return [cards count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isSearching){
        return (UITableViewCell*)[[filteredCards objectAtIndex:indexPath.row] getPropertyByName:@"cell"];
    }
    UITableViewCell *cell = (UITableViewCell*)[[cards objectAtIndex:indexPath.row] getPropertyByName:@"cell"];
    
    //Comment Block Check
    NSString* type = (NSString*)[[cards objectAtIndex:indexPath.row] getPropertyByName:@"type"];
    if ([type isEqualToString:@"commentBlock"]){
        NSDictionary *dict = (NSDictionary*)[[cards objectAtIndex:indexPath.row] getPropertyByName:@"dict"];
        return [self commentBlockCellForDict:dict inTable:tableView atIndexPath:indexPath];
    }
    else if ([type isEqualToString:@"header"]){
        HeaderCardCell *h = (HeaderCardCell*)cell;
        [h setDelegate:self];
    }
    
    return cell;
}

- (CommentBlockCell*)commentBlockCellForDict:(NSDictionary*)dict inTable:(UITableView*)tableView atIndexPath:(NSIndexPath*)indexPath{
    CommentBlockCell *cbCell = [tableView dequeueReusableCellWithIdentifier:@"commentBlock" forIndexPath:indexPath];
    [cbCell.cButton setupButton:dict];
    //[cbCell.likeButton setupButton:dict];
    [cbCell.cButton addTarget:self action:@selector(showDetailViewController:) forControlEvents:UIControlEventTouchUpInside];
    return cbCell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [(NSNumber*)[[cards objectAtIndex:indexPath.row] getPropertyByName:@"cellHeight"] floatValue];
}

#pragma mark - Status Bar Setup

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Cell Comment Button
- (void)showDetailViewController:(id)sender{
    SlideRightSegue *slide = [SlideRightSegue alloc];
    CommentViewController *commentView = [[CommentViewController alloc] init];
    NSDictionary *commentArr = (NSDictionary*)[(CommentButton*)sender getData];
    [commentView setCommentData:commentArr];
    [slide performSegueFrom:self to:commentView];
}

#pragma mark - Refresh Page

- (void)refresh:(id)sender{
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser
                                                         password:baseFrontdoorCredPass
                                                      persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred
                                                                  andURL:CARD_URL
                                                             andDelegate:self];
    [bc requestData];
    [self.tableView reloadData];
}

#pragma mark - NSURLSessionDelegate

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
   didReceiveData:(NSData *)data {
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    if (dictionary){
        dispatch_async(dispatch_get_main_queue(), ^{
            jsonHelp = [[JSONParser alloc] init];
            cards = [jsonHelp parseCardDict:dictionary];
            [self.refreshControl endRefreshing];
            [self.tableView reloadData];
       });
    }
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler{
    if ([challenge previousFailureCount] == 0) {
        NSLog(@"received and handle authentication challenge");
        NSURLCredential *newCredential = [NSURLCredential credentialWithUser:baseFrontdoorCredUser
                                                                    password:baseFrontdoorCredPass
                                                                 persistence:NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
    }
    else {
        NSLog(@"previous authentication failure");
    }
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error{
    NSLog(@"%@", error);
}

#pragma mark - New Post Delegate Methods
-(void)presentNewPost:(CardType)type{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    switch (type) {
        case kCardTask:{
            PostTaskViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"postTask"];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        case kCardVote:{
            PostVoteViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"postVote"];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        case kCardEvent:{
            PostEventViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"postEvent"];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        case kCardPayment:{
            PostPaymentViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"postPayment"];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        case kCardAnnouncement:{
            PostAnnouncementViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"postAnnouncement"];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        case kCardSubleaseRequest:
            break;
        case kCardSetup:
            break;
        default:
            break;
    }
}

@end
