//
//  PostTaskViewController.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/7/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardHandler.h"

@interface PostTaskViewController : UITableViewController <UITextViewDelegate>
{
    NSMutableArray *members;
    NSMutableArray *usernames;
    NSNumber *assignee;
    NSString *message;
}
@property(nonatomic, retain)IBOutlet UITextView *textView;
@property(nonatomic, retain)UIBarButtonItem *postButton;
@end

