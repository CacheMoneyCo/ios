//
//  PostVoteViewController.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/7/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "PostVoteViewController.h"

@interface PostVoteViewController ()

@end

@implementation PostVoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _postButton = [[UIBarButtonItem alloc]initWithTitle:@"Post" style:UIBarButtonItemStyleDone target:self action:@selector(postCard)];
    self.navigationItem.rightBarButtonItem = _postButton;
    [_postButton setEnabled:false];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelPost)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.34 green:0.65 blue:0.35 alpha:1.0]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    _textView.delegate = self;
    _textView.text = @"Write Vote Question...";
    _textView.textColor = [UIColor lightGrayColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)postCard{
    [CardHandler createCardWithType:kCardVote withInfo:@{ @"name": @"title", @"value": _textView.text }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelPost{
    [self.navigationController popViewControllerAnimated:true];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Write Vote Question..."]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Write Vote Question...";
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}

- (void)textViewDidChange:(UITextView *)textView{
    if ([textView.text length] > 140 || [textView.text length] == 0)
        [_postButton setEnabled:false];
    else
        [_postButton setEnabled:true];
}
@end
