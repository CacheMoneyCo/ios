//
//  PostPaymentViewController.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/8/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardHandler.h"

@interface PostPaymentViewController : UITableViewController <UITextViewDelegate, UITextFieldDelegate>
{
    NSMutableArray *members;
    NSMutableArray *usernames;
    NSNumber *assignee;
    NSString *message;
    NSNumber *amount;
}
@property(nonatomic, retain)IBOutlet UITextView *textView;
@property(nonatomic, retain)IBOutlet UITextField *textField;
@property(nonatomic, retain)UIBarButtonItem *postButton;
@end

