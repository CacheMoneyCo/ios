//
//  PostTaskViewController.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/7/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "PostTaskViewController.h"

@interface PostTaskViewController ()

@end

@implementation PostTaskViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    _postButton = [[UIBarButtonItem alloc]initWithTitle:@"Post" style:UIBarButtonItemStyleDone target:self action:@selector(postCard)];
    self.navigationItem.rightBarButtonItem = _postButton;
    [_postButton setEnabled:false];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelPost)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.34 green:0.65 blue:0.35 alpha:1.0]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    assignee = [NSNumber numberWithInteger:-1];
    members = [[NSMutableArray alloc] init];
    usernames = [[NSMutableArray alloc] init];
    message = [[NSString alloc]init];
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSDictionary *mems = [def objectForKey:@"houseMembers"];
    for (NSDictionary* m in mems){
        if ([[m objectForKey:@"first_name"] isEqualToString:@""] || [[m objectForKey:@"last_name"] isEqualToString:@""]){
            [members addObject:[NSString stringWithFormat:@"%@", [m objectForKey:@"username"]]];
            [usernames addObject:[NSString stringWithFormat:@"%@", [m objectForKey:@"username"]]];
        }
        else{
            [members addObject:[NSString stringWithFormat:@"%@ %@", [m objectForKey:@"first_name"], [m objectForKey:@"last_name"]]];
            [usernames addObject:[NSString stringWithFormat:@"%@", [m objectForKey:@"username"]]];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)postCard{
    if ([assignee isEqualToNumber:[NSNumber numberWithInteger:-1]]){
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Please select a housemate to assign the task."
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        [CardHandler createCardWithType:kCardTask withArray:@[@{ @"name": @"title", @"value": _textView.text }
                                                         , @{ @"name": @"assignee", @"value": [self userNameFromID] }]];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (NSString*)userNameFromID{
    return [usernames objectAtIndex:[assignee intValue]];
}

- (void)cancelPost{
    [self.navigationController popViewControllerAnimated:true];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Write Task..."]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Write Task...";
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}

- (void)textViewDidChange:(UITextView *)textView{
    message = textView.text;
    if ([textView.text length] > 140 || [textView.text length] == 0)
        [_postButton setEnabled:false];
    else
        [_postButton setEnabled:true];
}


#pragma mark - Table View Setup
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0: return 1;
            break;
        case 1: return [members count];
            break;
        default: return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"messageCell"];
    switch (indexPath.section) {
        case 0:{
            _textView = [cell viewWithTag:100];
            _textView.delegate = self;
            _textView.text = ([message isEqualToString:@""]) ? @"Write Task..." : message;
            _textView.textColor = ([message isEqualToString:@""]) ? [UIColor lightGrayColor] :  [UIColor blackColor];
        }
            break;
        case 1:{
            cell = [tableView dequeueReusableCellWithIdentifier:@"assigneeCell"];
            UILabel *name = [cell viewWithTag:100];
            [name setText:[NSString stringWithFormat:@"%@", [members objectAtIndex:indexPath.row]]];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            if ([assignee integerValue] == indexPath.row){
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
        }
            break;
        default:
            break;
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return (indexPath.section == 0)? 200:44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1){
        assignee = [NSNumber numberWithInteger:indexPath.row];
        [tableView reloadData];
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 1:
            return @"Assign to";
            break;
        default:
            return nil;
            break;
    }
}

@end
