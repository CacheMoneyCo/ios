//
//  UtilityFinderView.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/23/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "UtilityFinderView.h"

@interface UtilityFinderView ()

@end

@implementation UtilityFinderView

- (void)viewDidLoad {
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.34 green:0.65 blue:0.35 alpha:1.0]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [super viewDidLoad];
    sBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height)];
    [sBar setPlaceholder:@"Enter ZIP Code to find Utilities"];
    [sBar setKeyboardType:UIKeyboardTypeNumberPad];
    
    UIToolbar* searchToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    searchToolbar.barStyle = UIBarStyleDefault;
    searchToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Search" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [searchToolbar sizeToFit];
    sBar.inputAccessoryView = searchToolbar;
    UITextField *textField = [sBar valueForKey:@"_searchField"];
    [textField setDelegate:self];
    self.navigationItem.titleView = sBar;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    utiltiesArr = [[NSMutableArray alloc] init];
    cableArr = [[NSMutableArray alloc] init];
    internetArr = [[NSMutableArray alloc] init];
    homeSecArr = [[NSMutableArray alloc] init];
    [self.tableView reloadData];
    return true;
}

#pragma mark - Backend Functions

- (void)getLocalUtilities{
#warning Connect to Backend to get array
    utiltiesArr = [NSMutableArray arrayWithObjects:@"", nil];
}

- (void)getInternetProviders{
#warning Connect to Backend to get array
    internetArr = [NSMutableArray arrayWithObjects:@"", nil];
}

- (void)getCableProviders{
#warning Connect to Backend to get array
    cableArr = [NSMutableArray arrayWithObjects:@"", nil];
}

- (void)getHomeSecurityProviders{
#warning Connect to Backend to get array
    homeSecArr = [NSMutableArray arrayWithObjects:@"", nil];
}

#pragma mark - SearchBar Actions

-(void)cancelNumberPad{
    [sBar resignFirstResponder];
}

-(void)doneWithNumberPad{
    [sBar resignFirstResponder];
    [self getLocalUtilities];
    [self getInternetProviders];
    [self getCableProviders];
    [self getHomeSecurityProviders];
    NSLog(@"ZIP Code: %@", [sBar text]);
    [self.tableView reloadData];
}

#pragma mark - Table view data source

#pragma mark - Table View Setup
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    int sections = 0;
    if ([utiltiesArr count] > 0) sections++;
    if ([internetArr count] > 0) sections++;
    if ([cableArr count] > 0) sections++;
    if ([homeSecArr count] > 0) sections++;
    return sections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section { //utilities, internet, cable, home security
    switch (section) {
        case 0:
            return [utiltiesArr count];
            break;
        case 1:
            return [internetArr count];
            break;
        case 2:
            return [cableArr count];
            break;
        case 3:
            return [homeSecArr count];
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"emptyCell"];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *sectionName;
    switch (section){
        case 0:
            if ([utiltiesArr count] > 0) sectionName = @"UTILITIES";
            break;
        case 1:
            if ([internetArr count] > 0) sectionName = @"INTERNET";
            break;
        case 2:
            if ([cableArr count] > 0) sectionName = @"CABLE";
            break;
        case 3:
            if ([homeSecArr count] > 0) sectionName = @"HOME SECURITY";
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

@end
