//
//  HamburgerViewController.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/11/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExtrasCellObject.h"
#import "RentCalculationView.h"
#import "BackendConnector.h"
#import "JSONParser.h"
#import "HouseViewController.h"

@interface HamburgerViewController : UITableViewController
{
    NSMutableArray *cellArray;
    NSMutableArray *houseArr;
    JSONParser *jsonParser;
}

@end
