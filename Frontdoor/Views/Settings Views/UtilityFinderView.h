//
//  UtilityFinderView.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/23/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UtilityFinderView : UITableViewController <UISearchBarDelegate, UITextFieldDelegate>
{
    UISearchBar *sBar;
    NSMutableArray *internetArr;
    NSMutableArray *utiltiesArr;
    NSMutableArray *cableArr;
    NSMutableArray *homeSecArr;
}
@end
