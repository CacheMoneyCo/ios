//
//  NewRentViewController.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/3/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "BackendConnector.h"
#import "NewHouse.h"

@interface NewRentViewController : UITableViewController <MKMapViewDelegate, CLLocationManagerDelegate>
{
    NSMutableArray *bookmarkArr;
    UIRefreshControl *refControl;
    MKMapView *mapView;
    CLLocationManager *locationManager;
    CLLocation* house1;
}
@end
