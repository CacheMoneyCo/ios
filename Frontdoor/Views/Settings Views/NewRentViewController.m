//
//  NewRentViewController.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/3/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "NewRentViewController.h"
#define RENTABLE_URL @"http://127.0.0.1:8000/api/cards/"

@interface NewRentViewController ()

@end

@implementation NewRentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.34 green:0.65 blue:0.35 alpha:1.0]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.tableView.allowsSelection = FALSE;
    refControl = [[UIRefreshControl alloc] init];
    [refControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self setRefreshControl:refControl];
    
    locationManager = [CLLocationManager new];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [locationManager requestAlwaysAuthorization];
    }
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    bookmarkArr = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpMap{
    mapView.showsUserLocation = YES;
    mapView.mapType = MKMapTypeStandard;
    mapView.delegate = self;
    
    [mapView setShowsUserLocation:TRUE];
    
}

#pragma mark - Location Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    //house1 = location;
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:CLLocationCoordinate2DMake(37.3401, -122.02401)];
    [annotation setTitle:@"1643 Mariani Dr"];
    [annotation setSubtitle:@"5 Beds 2.5 Baths \n $5000/month"];
    [mapView addAnnotation:annotation];
    NSLog(@"lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
    
    //NSString *currentLatitude = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    //NSString *currentLongitude = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    [locationManager stopUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status)
    {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            [locationManager requestAlwaysAuthorization];
        }
            break;
        default:
        {
            [locationManager startUpdatingLocation];
        }
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Error while getting core location : %@",[error localizedFailureReason]);
    if ([error code] == kCLErrorDenied)
    {
        //you had denied
    }
    [manager stopUpdatingLocation];
}

#pragma mark - MapView Delegate

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.035;
    span.longitudeDelta = 0.035;
    CLLocationCoordinate2D location;
    location.latitude = aUserLocation.coordinate.latitude;
    location.longitude = aUserLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    [aMapView setRegion:region animated:YES];
}


- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]]){
        // Try to dequeue an existing pin view first (code not shown).
        
        // If no pin view already exists, create a new one.
        MKPinAnnotationView *customPinView = [[MKPinAnnotationView alloc]
                                              initWithAnnotation:annotation reuseIdentifier:@"housePin"];
        
        customPinView.animatesDrop = NO;
        customPinView.canShowCallout = YES;
        
        // Because this is an iOS app, add the detail disclosure button to display details about the annotation in another view.
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [rightButton addTarget:self action:@selector(addMapToArray:) forControlEvents:UIControlEventTouchUpInside];
        customPinView.rightCalloutAccessoryView = rightButton;
        
        // Add a custom image to the left side of the callout.
        UIImageView *myCustomImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"houseIcon-1"]];
        customPinView.leftCalloutAccessoryView = myCustomImage;
        
        return customPinView;
    }
    return nil;
}

- (void)addMapToArray:(id)sender{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Add House"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* addAction = [UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [bookmarkArr addObject:[NewHouse initWithAddress:@"1643 Mariani Dr" andNumBeds:[NSNumber numberWithInteger:5] andNumBaths:[NSNumber numberWithInteger:5] andRentAmount:[NSNumber numberWithInteger:5000]]];
        [self reloadTable];
    }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancelAction];
    [alert addAction:addAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)reloadTable{
    [self.tableView reloadData];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section){
        case 0: return 1; break;//profile
        case 1: return [bookmarkArr count];
            break;//houses
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = 0;
    for (int i = 0; i < indexPath.section; i++){
        row+= [tableView numberOfRowsInSection:i];
    }
    row+= indexPath.row;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"houseCell"];
    if (indexPath.row == 0 && indexPath.section == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"mapCell"];
        mapView = [cell viewWithTag:101];
        [self setUpMap];
    }
    else{
        NewHouse *n = [bookmarkArr objectAtIndex:indexPath.row];
        UILabel *title = [cell viewWithTag:100];
        [title setText:n.address];
        UILabel *subtitle = [cell viewWithTag:101];
        [subtitle setText:[NSString stringWithFormat:@"%li Beds %li Baths", [n.numBeds integerValue], [n.numBaths integerValue]]];
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 && indexPath.section == 0){
        return 389;
    }
    return 44;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *sectionName;
    switch (section){
        case 0:
            break;
        case 1:
            sectionName = ([bookmarkArr count] != 0) ? @"BOOKMARKED HOUSES" : nil;
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0)
        return 0.1;
    return tableView.sectionHeaderHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //count the number of rows in the sections ahead plus indexPath.row
    if (indexPath.section != 0){
        /*NSInteger row = 0;
        for (int i = 0; i < indexPath.section; i++){
            row+= [tableView numberOfRowsInSection:i];
        }
        row+= indexPath.row;
        ExtrasCellObject* cell = [cellArray objectAtIndex:row];
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NSLog(@"Row: %li, Identifier: %@, Title: %@", row, cell.identifier, cell.title);
        RentCalculationView* controller = [storyboard instantiateViewControllerWithIdentifier:cell.identifier];
        [self.navigationController pushViewController:controller animated:YES];*/
    }
}

#pragma mark - Refresh Page

- (void)refresh:(id)sender{
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser password:baseFrontdoorCredPass persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred andURL:RENTABLE_URL andDelegate:self];
    //[bc requestData];
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}


#pragma mark - NSURLSessionDelegate

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
   didReceiveData:(NSData *)data {
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    if (dictionary){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.refreshControl endRefreshing];
            [self.tableView reloadData];
        });
    }
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler{
    if ([challenge previousFailureCount] == 0) {
        NSLog(@"received and handle authentication challenge");
        NSURLCredential *newCredential = [NSURLCredential credentialWithUser:baseFrontdoorCredUser
                                                                    password:baseFrontdoorCredPass
                                                                 persistence:NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
    }
    else {
        NSLog(@"previous authentication failure");
        /*UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AuthViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"authViewController"];
        [self presentViewController:controller animated:YES completion:^(void){}];*/
    }
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error{
    NSLog(@"%@", error);
}


@end
