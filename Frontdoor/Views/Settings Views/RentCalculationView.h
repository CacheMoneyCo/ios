//
//  RentCalculationView.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/17/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomObject.h"
#import "BackendConnector.h"
#import "NewRoomViewController.h"
#import "HouseRooms.h"

@interface RentCalculationView : UITableViewController <UITextFieldDelegate>
{
    NSMutableArray *roomArr;
    BOOL usingCommon;
    NSNumber *totalRent;
    UISlider *commonAreaSize;
}
@end
