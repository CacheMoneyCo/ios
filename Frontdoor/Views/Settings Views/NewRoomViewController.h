//
//  NewRoomViewController.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/8/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BackendConnector.h"

@interface NewRoomViewController : UITableViewController{
    NSArray *dataSource;
    
    UITextField *nameField;
    UITextField *sqField;
    UITextField *numField;
    UISwitch *bathSwitch;
    UISwitch *awkSwitch;
    UISwitch *closetSwitch;
}
@property(nonatomic, retain)UIBarButtonItem *postButton;
@property(nonatomic, retain)NSMutableArray *roomArr;
- (void)addRoom;
@end
