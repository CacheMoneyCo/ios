//
//  NewRoomViewController.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/8/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "NewRoomViewController.h"
#define POST_HOUSE_URL [NSString stringWithFormat:@"%@room/add/", baseFrontdoorURL]

@interface NewRoomViewController ()

@end

@implementation NewRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _postButton = [[UIBarButtonItem alloc]initWithTitle:@"Create" style:UIBarButtonItemStyleDone target:self action:@selector(addRoom)];
    self.navigationItem.rightBarButtonItem = _postButton;
    //[_postButton setEnabled:false];
    dataSource = [NSArray arrayWithObjects:@"nameCell",
                  @"sqCell",
                  @"numResCell",
                  @"bathCell",
                  @"awkCell",
                  @"closetCell",
                  nil];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addRoom{
    //@{ @"name": @"title", @"value": _textView.text }
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser password:baseFrontdoorCredPass persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred andURL:POST_HOUSE_URL andDelegate:self];
    NSArray *params = @[@{ @"name": @"room_name", @"value": nameField.text },
                        @{ @"name": @"square_footage", @"value": sqField.text },
                        @{ @"name": @"number_of_residents", @"value": numField.text },
                        @{ @"name": @"has_bathroom", @"value": [NSNumber numberWithBool:bathSwitch.isOn] },
                        @{ @"name": @"has_awkward_layout", @"value": [NSNumber numberWithBool:awkSwitch.isOn] },
                        @{ @"name": @"has_closet", @"value": [NSNumber numberWithBool:closetSwitch.isOn] }];
    [bc postWithParams:params];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section){
        case 0: return 1; break;
        case 1: return 2; break;
        case 2: return 3; break;
        default:return 0;break;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = 0;
    for (int i = 0; i < indexPath.section; i++){
        row+= [tableView numberOfRowsInSection:i];
    }
    row+= indexPath.row;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[dataSource objectAtIndex:row] forIndexPath:indexPath];
    if (indexPath.section == 0){
        nameField = [cell viewWithTag:100];
    }
    else if (indexPath.section == 1){
        switch (indexPath.row) {
            case 0:
                sqField = [cell viewWithTag:100];
                break;
            case 1:
                numField = [cell viewWithTag:100];
                break;
        }
    }
    else if (indexPath.section == 2){
        switch (indexPath.row) {
            case 0:
                bathSwitch = [cell viewWithTag:100];
                break;
            case 1:
                awkSwitch = [cell viewWithTag:100];
                break;
            case 2:
                closetSwitch = [cell viewWithTag:100];
                break;
        }
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *sectionName;
    switch (section){
        case 0:
            sectionName = @"NAME";
            break;
        case 1:
            sectionName = @"PROPERTIES";
            break;
        case 2:
            sectionName = @"OPTIONS";
            break;
        default:
            sectionName = nil;
            break;
    }
    return sectionName;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

#pragma mark - NSURLSessionDelegate

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
   didReceiveData:(NSData *)data {
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    if (dictionary){
        dispatch_async(dispatch_get_main_queue(), ^{
           
        });
    }
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler{
    if ([challenge previousFailureCount] == 0) {
        NSLog(@"received and handle authentication challenge");
        NSURLCredential *newCredential = [NSURLCredential credentialWithUser:baseFrontdoorCredUser
                                                                    password:baseFrontdoorCredPass
                                                                 persistence:NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
    }
    else {
        NSLog(@"previous authentication failure");
    }
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error{
    NSLog(@"%@", error);
}
@end
