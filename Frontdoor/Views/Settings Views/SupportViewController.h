//
//  SupportViewController.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/3/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportViewController : UITableViewController
@property (nonatomic, retain)IBOutlet UITextField *nameField;
@property (nonatomic, retain)IBOutlet UITextField *emailField;
@property (nonatomic, retain)IBOutlet UITextField *subjectField;
@property (nonatomic, retain)IBOutlet UITextView *messageView;
- (IBAction)submitForm:(id)sender;
@end
