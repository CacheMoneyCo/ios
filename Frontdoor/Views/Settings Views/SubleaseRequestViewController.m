//
//  SubleaseRequestViewController.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/3/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "SubleaseRequestViewController.h"

@interface SubleaseRequestViewController ()

@end

@implementation SubleaseRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.34 green:0.65 blue:0.35 alpha:1.0]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    sPickerShowing = false;
    sDatePicker = [[UIDatePicker alloc]init];
    [sDatePicker setDate:[NSDate dateWithTimeIntervalSinceNow:0]];
    ePickerShowing = false;
    eDatePicker = [[UIDatePicker alloc]init];
    [eDatePicker setDate:[NSDate dateWithTimeIntervalSinceNow:2628000]];
    
    //self.tableView.allowsSelection = FALSE;
    tableArr = [[NSMutableArray alloc]
            initWithObjects:@"startDateLabel",@"endDateLabel",@"petsCell",@"sharedCell",@"smokeCell",@"messageCell",@"subCell", nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateStartLabel:(id)sender{
    UIDatePicker *datePick = (UIDatePicker*)sender;
    [_startDateLabel setText:[self stringFromDate:[datePick date]]];
}

- (void)updateEndLabel:(id)sender{
    UIDatePicker *datePick = (UIDatePicker*)sender;
    [_endDateLabel setText:[self stringFromDate:[datePick date]]];
}

- (void)submitForm:(id)sender{
    if ([_messageView.text isEqualToString:@""]){
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Please add a Message"
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Request Submitted!"
                                                                       message:@"You will recieve an email confirmation."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* doneAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self.navigationController popViewControllerAnimated:true];
        }];
        
        [alert addAction:doneAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section){
        case 0: return (sPickerShowing || ePickerShowing)? 3:2; break;
        case 1: return 3; break;
        case 2: return 1; break;
        case 3: return 1; break;
        default:
            return 0;
            break;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *sectionName;
    switch (section){
        case 0:
            sectionName = @"REQUESTED DATES";
            break;
        case 1:
            sectionName = @"PREFERENCES";
            break;
        case 2:
            sectionName = @"MESSAGE";
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = 0;
    for (int i = 0; i < indexPath.section; i++){
        row+= [tableView numberOfRowsInSection:i];
    }
    row+= indexPath.row;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[tableArr objectAtIndex:row] forIndexPath:indexPath];
    if (indexPath.section != 0){
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        if (indexPath.section == 2 && indexPath.row == 0){
            _messageView = [cell viewWithTag:100];
        }
        else if (indexPath.section == 3 && indexPath.row == 0){
            UIButton *submit = [cell viewWithTag:100];
            [submit addTarget:self action:@selector(submitForm:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    else{
        if (indexPath.row == 0){
            _startDateLabel = [cell viewWithTag:101];
            [_startDateLabel setText:[self stringFromDate:[sDatePicker date]]];
        }
        else if (indexPath.row == 1 && sPickerShowing){
            sDatePicker = [cell viewWithTag:100];
            [sDatePicker addTarget:self action:@selector(updateStartLabel:) forControlEvents:UIControlEventValueChanged];
        }
        else if (indexPath.row == 2 && ePickerShowing){
            eDatePicker = [cell viewWithTag:100];
            [eDatePicker addTarget:self action:@selector(updateEndLabel:) forControlEvents:UIControlEventValueChanged];
        }
        else if (indexPath.row == 1 || (sPickerShowing && indexPath.row == 2)){
            _endDateLabel = [cell viewWithTag:101];
            [_endDateLabel setText:[self stringFromDate:[eDatePicker date]]];
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (sPickerShowing && indexPath.section == 0 && indexPath.row == 1){
        return 162;
    }
    if (ePickerShowing && indexPath.section == 0 && indexPath.row == 2){
        return 162;
    }
    else if (indexPath.section == 2)
        return 270;
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{//clicking start when end picker is visible
    if (indexPath.row == 0 && !sPickerShowing && !ePickerShowing){
        [tableArr insertObject:@"startDatePicker" atIndex:indexPath.row+1];
        sPickerShowing = true;
        ePickerShowing = false;
    }
    else if(indexPath.row == 0 && sPickerShowing){
        [tableArr removeObjectAtIndex:indexPath.row+1];
        sPickerShowing = false;
        ePickerShowing = false;
    }
    else if(indexPath.row == 0 && ePickerShowing){
        [tableArr removeObjectAtIndex:indexPath.row+2];
        [tableArr insertObject:@"startDatePicker" atIndex:indexPath.row+1];
        sPickerShowing = true;
        ePickerShowing = false;
    }
    else if (indexPath.row == 2 && sPickerShowing){
        [tableArr removeObjectAtIndex:indexPath.row-1];
        [tableArr insertObject:@"endDatePicker" atIndex:indexPath.row];
        sPickerShowing = false;
        ePickerShowing = true;
    }
    else if (indexPath.row == 1 && !sPickerShowing && !ePickerShowing){
        [tableArr insertObject:@"endDatePicker" atIndex:indexPath.row+1];
        sPickerShowing = false;
        ePickerShowing = true;
    }
    else if (indexPath.row == 1 && ePickerShowing){
        [tableArr removeObjectAtIndex:indexPath.row+1];
        sPickerShowing = false;
        ePickerShowing = false;
    }
    [tableView reloadData];
}

- (NSString*)stringFromDate:(NSDate*)date{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEEE, MMM d, yyyy"];
    return [dateFormat stringFromDate:date];
}

@end
