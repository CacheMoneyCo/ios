//
//  RentCalculationView.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/17/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "RentCalculationView.h"
#define POST_RENT_URL [NSString stringWithFormat:@"%@rent/", baseFrontdoorURL]

@interface RentCalculationView ()

@end

@implementation RentCalculationView

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *plusButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addRoom)];
    self.navigationItem.rightBarButtonItem = plusButton;
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.34 green:0.65 blue:0.35 alpha:1.0]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [[[HouseRooms alloc] init] getRooms];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    roomArr = [[NSMutableArray alloc] initWithArray:[self getRoomArrayFromBackend]];
    [self.tableView reloadData];
}

#pragma mark - Room Methods
-(void)addRoom{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NewRoomViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"makeNewRoom"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)usingCommonSpace:(UISwitch *)theSwitch{
    usingCommon = theSwitch.isOn;
    [self.tableView reloadData];
}


- (NSMutableArray*)getRoomArrayFromBackend{
    totalRent = [NSNumber numberWithInt:0];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSDictionary *rooms = [def objectForKey:@"houseRooms"];
    for(NSDictionary* room in rooms){
        NSString *name = [NSString stringWithFormat:@"%@", [room objectForKey:@"name"]];
        NSNumber *sq = [NSNumber numberWithInteger:[[room objectForKey:@"squarefeet"] integerValue]];
        NSNumber *num = [NSNumber numberWithInteger:[[room objectForKey:@"num_users"] integerValue]];
        NSNumber *bath = [NSNumber numberWithInteger:[[room objectForKey:@"hasbathroom"] boolValue]];
        NSNumber *awk = [NSNumber numberWithInteger:[[room objectForKey:@"hasawkwardlayout"] boolValue]];
        NSNumber *closet = [NSNumber numberWithInteger:[[room objectForKey:@"hascloset"] boolValue]];
        NSNumber *rent = [NSNumber numberWithInteger:[[room objectForKey:@"rent"] integerValue]];
        totalRent = [NSNumber numberWithInteger:[totalRent integerValue] + ([rent integerValue] * [num integerValue])];
        [arr addObject:[RoomObject initWithName:name
                                      sqFootage:sq
                                 numInhabitants:num
                                           bath:bath
                                            awk:awk
                                         closet:closet
                                           rent:rent]];
    }
    return arr;
}

- (void)pushRent{
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser password:baseFrontdoorCredPass persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred andURL:POST_RENT_URL andDelegate:self];
    NSArray *params = (usingCommon)? @[@{ @"name": @"total_rent", @"value": totalRent },
                        @{ @"name": @"include_common_space", @"value": @"True" },
                        @{ @"name": @"common_space_importance", @"value": [NSNumber numberWithDouble:commonAreaSize.value] }]
                        :@[@{ @"name": @"total_rent", @"value": totalRent },
                        @{ @"name": @"include_common_space", @"value": @"False" },
                        @{ @"name": @"common_space_importance", @"value": [NSNumber numberWithDouble:commonAreaSize.value] }];;
    [bc postWithParamsAndRoomRefresh:params completionHandler:^(){
        [[[HouseRooms alloc] init] getRoomsWithWithCompletionHandler:^(){
            dispatch_async(dispatch_get_main_queue(), ^{
                roomArr = [[NSMutableArray alloc] initWithArray:[self getRoomArrayFromBackend]];
                [self.tableView reloadData];
            });
        }];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    totalRent = [NSNumber numberWithInteger:[textField.text integerValue]];
    [self pushRent];
}

-(void)dismissKeyboard:(id)sender{
    [(UITextField*)sender resignFirstResponder];
}

#pragma mark - Table View Setup
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger sec = 2;//always gonna have the rent amount cell, and the common area cell
    sec += [roomArr count];
    return sec;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section){
        case 0: return 1; break;//Rent Amount Cell
        case 1://rent amount
            //if ([roomArr count] == 0) return 1;
            return [roomArr count];
            break;
        case 2:
            return (usingCommon) ? 2 : 1;
        break;//Common Space Cell
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"roomCell"];
    if (indexPath.section == 0 && indexPath.row == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"rentAmount"];
        UITextField* tf = (UITextField*)[cell viewWithTag:200];
        [tf setText: [NSString stringWithFormat:@"%li",[totalRent integerValue]]];
        [tf addTarget:self action:@selector(dismissKeyboard:) forControlEvents:UIControlEventEditingDidEnd];
        [tf addTarget:self action:@selector(dismissKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
        [tf setDelegate:self];
    }
    else if (indexPath.section == 2 && indexPath.row == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"commonCell"];
        UISwitch* sw = (UISwitch*)[cell viewWithTag:100];
        [sw setOn:usingCommon];
        [sw addTarget:self action:@selector(usingCommonSpace:) forControlEvents:UIControlEventValueChanged];
        [sw addTarget:self action:@selector(pushRent) forControlEvents:UIControlEventValueChanged];
    }
    else if (indexPath.section == 2 && indexPath.row == 1){
        cell = [tableView dequeueReusableCellWithIdentifier:@"commonSpaceSlider"];
        commonAreaSize = [cell viewWithTag:200];
        [commonAreaSize addTarget:self action:@selector(pushRent) forControlEvents:UIControlEventValueChanged];
    }
    else if (indexPath.section == 1){
        if ([roomArr count] == 0){
            cell = [tableView dequeueReusableCellWithIdentifier:@"emptyHouse"];
        }
        else{
            RoomObject *rm = (RoomObject*)[roomArr objectAtIndex:indexPath.row];
            UILabel *name = (UILabel*)[cell viewWithTag:100];
            [name setText:rm.name];
            UILabel *sq = (UILabel*)[cell viewWithTag:101];
            [sq setText:[NSString stringWithFormat:@"%li sq ft", [rm.sqFootage integerValue]]];
            UILabel *rent = (UILabel*)[cell viewWithTag:102];
            [rent setText:[NSString stringWithFormat:@"$%li", [rm.rent integerValue]]];
            if ([rm.numInhabitant integerValue] > 1){
                [rent setText:[NSString stringWithFormat:@"$%li/%li People", ([rm.rent integerValue]* [rm.numInhabitant integerValue]), [rm.numInhabitant integerValue] ]];
            }
            rent.adjustsFontSizeToFitWidth=YES;
            rent.minimumScaleFactor=0.5;
        }
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1)
        return YES;
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1)
        return YES;
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1)
        return 61;
    else if (indexPath.section == 2 && indexPath.row == 1)
        return 70;
    return 44;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *sectionName;
    switch (section){
        case 0:
            sectionName = @"TOTAL RENT";
            break;
        case 1:
            sectionName = @"ROOMS";
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

@end
