//
//  HouseViewController.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/3/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "HouseViewController.h"
#define GET_HOUSE_URL [NSString stringWithFormat:@"%@leases/", baseFrontdoorURL]
#define POST_HOUSE_URL [NSString stringWithFormat:@"%@lease/change/", baseFrontdoorURL]
@interface HouseViewController ()

@end

@implementation HouseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.34 green:0.65 blue:0.35 alpha:1.0]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self setTitle:_house.name];
    [[[HouseMembers alloc] init] getMembers];
    [[[HouseRooms alloc] init] getRooms];
    membersArr = [self getMembers];
    roomArr = [self getRooms];
    refControl = [[UIRefreshControl alloc] init];
    [refControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self setRefreshControl:refControl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableArray*)getMembers{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSDictionary *members = [def objectForKey:@"houseMembers"];
    for(NSDictionary* member in members){
        if ([[member objectForKey:@"first_name"] isEqualToString:@""] || [[member objectForKey:@"last_name"] isEqualToString:@""]){
            [arr addObject:[NSString stringWithFormat:@"%@", [member objectForKey:@"username"]]];
        }
        else{
            [arr addObject:[NSString stringWithFormat:@"%@ %@", [member objectForKey:@"first_name"], [member objectForKey:@"last_name"]]];
        }
    }
    return arr;
}

- (NSMutableArray*)getRooms{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSDictionary *rooms = [def objectForKey:@"houseRooms"];
    for(NSDictionary* room in rooms){
        [arr addObject:[NSString stringWithFormat:@"%@", [room objectForKey:@"name"]]];
    }
    return arr;
}

- (void)deleteHouse{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Are you sure?"
                                                                   message:@"This action is irreversible."
                                                            preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* delAction = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        
    }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancelAction];
    [alert addAction:delAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)setHouse{
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser password:baseFrontdoorCredPass persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred andURL:POST_HOUSE_URL andDelegate:self];
    NSString *hID = [NSString stringWithFormat:@"%li", [_house.hID integerValue]];
    NSLog(@"ID: %@", hID);
    NSArray* params = @[ @{ @"name": @"lease_id", @"value": hID }];
    [bc postWithParams:params];
    [self refresh:self];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"House set to %@", _house.name]
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Ok!" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
    [self.tableView reloadData];
}

- (void)refresh:(id)sender{
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser password:baseFrontdoorCredPass persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred andURL:GET_HOUSE_URL andDelegate:self];
    [bc requestData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section){
        case 0: return [membersArr count]; break;//Members
        case 1: return [roomArr count]; break;//Rooms
        case 2: return 1; break;//Set to Default
        case 3: return 1; break;//Delete
        default:return 0;break;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"basicCell" forIndexPath:indexPath];
    if (indexPath.section == 2){
        cell = [tableView dequeueReusableCellWithIdentifier:@"setHouse" forIndexPath:indexPath];
        UIButton *set = [cell viewWithTag:100];
        if ([_house.isCurrent boolValue]){
            [set setTitle:@"Already Current House" forState:UIControlStateNormal];
            [set setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        }
        else{
            [set addTarget:self action:@selector(setHouse) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    else if (indexPath.section == 3){
        cell = [tableView dequeueReusableCellWithIdentifier:@"deleteHouse" forIndexPath:indexPath];
        UIButton *del = [cell viewWithTag:100];
        [del addTarget:self action:@selector(deleteHouse) forControlEvents:UIControlEventTouchUpInside];
    }
    else{
        UIImageView *image = [cell viewWithTag:100];
        UILabel *label = [cell viewWithTag:101];
        if (indexPath.section == 1){
            [image setImage:[UIImage imageNamed:@"roomIcon"]];
            [label setText:[roomArr objectAtIndex:indexPath.row]];
        }
        else if (indexPath.section == 0){
            [label setText:[membersArr objectAtIndex:indexPath.row]];
        }
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *sectionName;
    switch (section){
        case 0:
            sectionName = @"MEMBERS";
            break;
        case 1:
            sectionName = @"ROOM";
            break;
        default:
            sectionName = nil;
            break;
    }
    return sectionName;
}

#pragma mark - NSURLSessionDelegate

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
   didReceiveData:(NSData *)data {
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    if (dictionary){
        dispatch_async(dispatch_get_main_queue(), ^{
            JSONParser* jp = [[JSONParser alloc]init];
            NSNumber *hID = [NSNumber numberWithInteger:[_house.hID integerValue]];
            _house = [jp getHouseInfoWithID:hID fromDict:dictionary];
            [self.tableView reloadData];
            [self.refreshControl endRefreshing];
        });
    }
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler{
    if ([challenge previousFailureCount] == 0) {
        NSLog(@"received and handle authentication challenge");
        NSURLCredential *newCredential = [NSURLCredential credentialWithUser:baseFrontdoorCredUser
                                                                    password:baseFrontdoorCredPass
                                                                 persistence:NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
    }
    else {
        NSLog(@"previous authentication failure");
    }
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error{
    NSLog(@"%@", error);
}


@end
