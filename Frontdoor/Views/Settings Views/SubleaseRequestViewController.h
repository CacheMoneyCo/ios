//
//  SubleaseRequestViewController.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/3/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubleaseRequestViewController : UITableViewController
{
    NSMutableArray *tableArr;
    bool sPickerShowing;
    bool ePickerShowing;
    NSDate *startDate;
    NSDate *endDate;
    UIDatePicker *sDatePicker;
    UIDatePicker *eDatePicker;
}
@property (nonatomic, retain)UILabel *startDateLabel;
@property (nonatomic, retain)UILabel *endDateLabel;
@property (nonatomic, retain)UITextView *messageView;
@end
