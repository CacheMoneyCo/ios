//
//  HouseViewController.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/3/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BackendConnector.h"
#import "HouseObject.h"
#import "JSONParser.h"

@interface HouseViewController : UITableViewController
{
    NSMutableArray *membersArr;
    NSMutableArray *roomArr;
    NSNumber *houseID;
    UIRefreshControl *refControl;
}
@property(nonatomic, retain)HouseObject *house;
//@property(nonatomic, retain)NSNumber *hID;
@end
