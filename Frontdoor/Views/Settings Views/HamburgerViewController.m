//
//  HamburgerViewController.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/11/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "HamburgerViewController.h"
#include <stdlib.h>
#define GET_HOUSE_URL [NSString stringWithFormat:@"%@leases/", baseFrontdoorURL]

@interface HamburgerViewController ()

@end

@implementation HamburgerViewController

- (void)viewDidAppear:(BOOL)animated{
    [self getHousesFromBackend];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateCellArr];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.tableView setShowsHorizontalScrollIndicator:NO];
    [self.tableView setShowsVerticalScrollIndicator:NO];
    [self.tableView reloadData];
    // Do any additional setup after loading the view.
}

- (void)getHousesFromBackend{
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser password:baseFrontdoorCredPass persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred andURL:GET_HOUSE_URL andDelegate:self];
    [bc requestData];
}

- (void)updateCellArr{
    cellArray = [[NSMutableArray alloc] initWithObjects:
                 @"viewProfile", nil];
    
    [cellArray addObjectsFromArray:houseArr];
    
    [cellArray addObject:[ExtrasCellObject initWithTitle:@"Find Utilities" andImageNamed:@"utilitiesIcon" andIdentifier:@"utilitiesView"]];
    [cellArray addObject:[ExtrasCellObject initWithTitle:@"Create Sublease Request" andImageNamed:@"subleaseIcon" andIdentifier:@"subleaseView"]];
    [cellArray addObject:[ExtrasCellObject initWithTitle:@"Find New House to Rent" andImageNamed:@"addHouseIcon" andIdentifier:@"newHouseView"]];
    [cellArray addObject:[ExtrasCellObject initWithTitle:@"Calculate Rent" andImageNamed:@"moneyIcon" andIdentifier:@"rentCalculator"]];
    
    [cellArray addObject:[ExtrasCellObject initWithTitle:@"Help and Support" andImageNamed:@"supportIcon" andIdentifier:@"frontdoorSupport"]];
    [cellArray addObject:[ExtrasCellObject initWithTitle:@"Settings" andImageNamed:@"settingsImage" andIdentifier:@"settingsView"]];
    
    [cellArray addObject:@"logout"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addHouse{
    __block UITextField *nameField;

    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Add New House"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField* textField){
        [textField setPlaceholder:@"House Name"];
        nameField = textField;
    }];

    UIAlertAction* addAction = [UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        NSString *imgName = [NSString stringWithFormat:@"houseIcon-%i", arc4random_uniform(3)];
        [houseArr addObject:[ExtrasCellObject initWithTitle:[nameField text] andImageNamed:imgName andIdentifier:@"myHouse"]];
        [self updateCellArr];
        [self.tableView reloadData];
    }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancelAction];
    [alert addAction:addAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)saveCustomObject:(ExtrasCellObject *)object key:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
    
}

- (ExtrasCellObject *)loadCustomObjectWithKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    ExtrasCellObject *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}

#pragma mark - Table View Setup
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section){
        case 0: return 1; break;//profile
        case 1: return [houseArr count]; break;//houses
        case 2: return 4; break;//extras
        case 3: return 2; break;//support
        case 4: return 1; break;//logout
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = 0;
    for (int i = 0; i < indexPath.section; i++){
        row+= [tableView numberOfRowsInSection:i];
    }
    row+= indexPath.row;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"actionCell"];
    if (indexPath.row == 0 && indexPath.section == 0){
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        cell = [tableView dequeueReusableCellWithIdentifier:[cellArray objectAtIndex:0]];
        UILabel *name = [cell viewWithTag:100];
        NSDictionary *member = [def objectForKey:@"member_self"];
        [name setText:[NSString stringWithFormat:@"%@ %@", [member objectForKey:@"first_name"], [member objectForKey:@"last_name"]]];
        cell.userInteractionEnabled = false;
    }
    else if (indexPath.row == 0 && indexPath.section == 4){
        cell = [tableView dequeueReusableCellWithIdentifier:[cellArray objectAtIndex:[cellArray count]-1]];
    }
    else if (indexPath.section == 1){
        HouseObject *h = (HouseObject*)[houseArr objectAtIndex:indexPath.row];
        UIImageView *imageView = [cell viewWithTag:100];
        [imageView setImage:[UIImage imageNamed:h.imageName]];
        UILabel *name = [cell viewWithTag:101];
        [name setText:h.name];
        if ([h.isCurrent boolValue] == 1) [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        else [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    else{
        UIImageView *imageView = [cell viewWithTag:100];
        ExtrasCellObject *eCell = (ExtrasCellObject*)[cellArray objectAtIndex:row];
        [imageView setImage:eCell.image];
        [imageView setTintColor:[UIColor redColor]];
        UILabel *actionTitle = [cell viewWithTag:101];
        [actionTitle setText:eCell.title];
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 && indexPath.section == 0){
        return 70;
    }
    return 44;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *sectionName;
    switch (section){
        case 0:
            break;
        case 1:
            sectionName = @"HOUSES";
            break;
        case 2:
            sectionName = @"EXTRAS";
            break;
        case 3:
            sectionName = @"SUPPORT";
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0)
        return 0.1;
    return tableView.sectionHeaderHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //count the number of rows in the sections ahead plus indexPath.row
    if (indexPath.section != 0){
        NSInteger row = 0;
        for (int i = 0; i < indexPath.section; i++){
            row+= [tableView numberOfRowsInSection:i];
        }
        row+= indexPath.row;
        NSString *identifier;
        if (indexPath.section != 1){
            ExtrasCellObject* cell = [cellArray objectAtIndex:row];
            identifier = cell.identifier;
        }
        else{
            HouseObject *h = [cellArray objectAtIndex:row];
            identifier = h.viewId;
        }
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        HouseViewController* controller = [storyboard instantiateViewControllerWithIdentifier:identifier];
        if (indexPath.section == 1){
            controller.house = [houseArr objectAtIndex:indexPath.row];
        }
        [self.navigationController pushViewController:controller animated:YES];
    }
}


- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    //NSLog(@"Section: %li", section);
    if (section == 1) {
        if ([view.subviews.lastObject isKindOfClass:[UIButton class]]) {
            return;
        }
        UIButton *addButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
        [addButton addTarget:self action:@selector(addHouse) forControlEvents:UIControlEventTouchUpInside];
        [addButton setTintColor:[UIColor colorWithRed:0.46 green:0.46 blue:0.47 alpha:1.00]];
        [view addSubview:addButton];
        
        // Place button on far right margin of header
        addButton.translatesAutoresizingMaskIntoConstraints = NO; // use autolayout constraints instead
        [addButton.trailingAnchor constraintEqualToAnchor:view.layoutMarginsGuide.trailingAnchor].active = YES;
        [addButton.bottomAnchor constraintEqualToAnchor:view.layoutMarginsGuide.bottomAnchor].active = YES;
    }
}

#pragma mark - NSURLSessionDelegate

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
   didReceiveData:(NSData *)data {
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    if (dictionary){
        dispatch_async(dispatch_get_main_queue(), ^{
            jsonParser = [[JSONParser alloc] init];
            houseArr = [jsonParser parseHouseDict:dictionary];
            [self updateCellArr];
            [self.tableView reloadData];
        });
    }
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler{
    if ([challenge previousFailureCount] == 0) {
        NSLog(@"received and handle authentication challenge");
        NSURLCredential *newCredential = [NSURLCredential credentialWithUser:baseFrontdoorCredUser
                                                                    password:baseFrontdoorCredPass
                                                                 persistence:NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
    }
    else {
        NSLog(@"previous authentication failure");
    }
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error{
    NSLog(@"%@", error);
}


@end
