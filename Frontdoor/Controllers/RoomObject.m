//
//  RoomObject.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/17/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "RoomObject.h"

@implementation RoomObject
+ (RoomObject*)initWithName:(NSString*)nm sqFootage:(NSNumber*)sq numInhabitants:(NSNumber*)ni bath:(NSNumber*)b awk:(NSNumber*)a closet:(NSNumber*)c rent:(NSNumber*)r{
    RoomObject* rm = [[RoomObject alloc] init];
    rm.name = nm;
    rm.sqFootage = sq;
    rm.numInhabitant = ni;
    rm.hasBath = b;
    rm.hasAwkLayout = a;
    rm.hasCloset = c;
    rm.rent = r;
    return rm;
}
@end
