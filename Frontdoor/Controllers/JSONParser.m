//
//  JSONParser.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "JSONParser.h"

@implementation JSONParser

#pragma mark -
#pragma mark Card Helper Methods

- (NSMutableArray*)parseCardDict:(NSDictionary*)dict{
    //NSMutableArray *array = [dict objectForKey:@"cards"];
    NSMutableArray *cards = [[NSMutableArray alloc] init];
    if (dict != NULL){
        [cards addObject:[[CardHandler alloc] initWithHeader]];
        [cards addObject:[[CardHandler alloc] initWithSpacer]];
        int card_id = 3;
        for(NSDictionary *cardData in dict){
            if ([cardData objectForKey:@"content"] != [NSNull null]){
                //NSLog(@"Card ID: %@", [cardData objectForKey:@"id"]);
                CardHandler *card = [[CardHandler alloc] initWithJSONDictionary:cardData];
                [cards addObject:card];
                if ([[cardData objectForKey:@"content"] objectForKey:@"comments"]){
                    CardHandler *comment = [[CardHandler alloc] initWithCommentBlockData:[cardData objectForKey:@"content"]];
                    [cards addObject: comment];
                }
                [cards addObject:[[CardHandler alloc] initWithSpacer]];
                card_id++;
            }
        }
        return cards;
    }
    return [[NSMutableArray alloc] initWithObjects:[[CardHandler alloc] initWithEmptyCardSet], nil];
}

- (NSDate*)convertDateAndTimeFromJSON:(NSString*)str{
    NSString *dateStr = [[str componentsSeparatedByString:@"T"] objectAtIndex:0];
    NSString *timeStr = [[[[str componentsSeparatedByString:@"T"] objectAtIndex:1] componentsSeparatedByString:@"."] objectAtIndex:0];
    NSString *comboStr = [[dateStr stringByAppendingString:@"-"] stringByAppendingString:timeStr];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd-HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:comboStr];
    return date;
}

- (NSDate*)convertDateFromJSON:(NSString*)str{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:str];
    return date;
}

- (NSDate*)convertTimeFromJSON:(NSString*)str{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:str];
    return date;
}


#pragma mark -
#pragma mark Chat Helper Methods

- (NSMutableArray*)parseChatDict:(NSDictionary*)dict{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (NSDictionary *info in dict){
        ChatObject *c = [[ChatObject alloc] initWithProperties:info];
        [arr insertObject:c atIndex:0];
    }
    return arr;
}



#pragma mark -
#pragma mark House Helper

- (NSMutableArray*)parseHouseDict:(NSDictionary*)dict{
    NSMutableArray *houses = [[NSMutableArray alloc] init];
    if (dict != NULL){
        for(NSDictionary *houseData in dict){
            [houses addObject:[self houseFromData:houseData]];
        }
        return houses;
    }
    return [[NSMutableArray alloc] init];
}

- (HouseObject*)getHouseInfoWithID:(NSNumber*)hID fromDict:(NSDictionary*)dict{
    HouseObject *empty = [[HouseObject alloc]init];
    for (NSDictionary *h in dict){
        if ([[h objectForKey:@"id"] isEqual:hID]){
            return [self houseFromData:h];
        }
    }
    return empty;
}

- (HouseObject*)houseFromData:(NSDictionary*)houseData{
    NSMutableArray *info = [[NSMutableArray alloc] init];
    [info addObject:[houseData objectForKey:@"id"]];//0
    [info addObject:[houseData objectForKey:@"house_name"]];//1
    [info addObject:[houseData objectForKey:@"start_date"]];//2
    [info addObject:[houseData objectForKey:@"end_date"]];//3
    NSNumber *landlord = ([houseData objectForKey:@"landlord"] == [NSNull null]) ? [NSNumber numberWithBool:false] : [NSNumber numberWithBool:true];
    [info addObject:landlord];//4
    NSNumber *curr =  [houseData objectForKey:@"is_current"];
    [info addObject:curr];//5
    [info addObject:@"myHouse"];//6
    [info addObject:@"houseIcon-0"];//7
    return [HouseObject houseWithProperties:info];
}
@end
