//
//  SlideRightSegue.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/31/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "SlideRightSegue.h"

@implementation SlideRightSegue

- (void) performSegueFrom:(UIViewController*)src to:(UIViewController*)dst {
    [UIView transitionWithView:src.navigationController.view duration:0.2
                       options:UIViewAnimationOptionShowHideTransitionViews
                    animations:^{
                        [src.navigationController pushViewController:dst animated:NO];
                    }
                    completion:NULL];
}

@end
