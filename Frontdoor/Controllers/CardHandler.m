//
//  CardHandler.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/14/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "CardHandler.h"
#define CARD_CREATE_URL [NSString stringWithFormat:@"%@card/create/", baseFrontdoorURL]

@implementation CardHandler

- (id)initWithJSONDictionary:(NSDictionary *)jsonDictionary {//dict: id, title, date, house     content: type, owner
    if(self = [self init]) {
        jsonHelp = [[JSONParser alloc] init];
        _dict = jsonDictionary;
        _cardID = [jsonDictionary objectForKey:@"id"];
        _title = [jsonDictionary objectForKey:@"title"];
        _house = [jsonDictionary objectForKey:@"house"];
        _date = [jsonHelp convertDateAndTimeFromJSON:[jsonDictionary objectForKey:@"date"]];
        if ([jsonDictionary objectForKey:@"content"] != [NSNull null]){
            NSDictionary *content = [jsonDictionary objectForKey:@"content"];
            if ([content objectForKey:@"type"] != [NSNull null])
                _type = [content objectForKey:@"type"];
            if ([content objectForKey:@"owner"] != [NSNull null])
                _owner = [content objectForKey:@"owner"];
            if ([content objectForKey:@"comments"] != [NSNull null])
                _comments = [content objectForKey:@"comments"];
            if ([content objectForKey:@"likes"] != [NSNull null])
                _likes = [content objectForKey:@"likes"];
            if ([content objectForKey:@"yes"] != [NSNull null])
                _yesVotes = [content objectForKey:@"yes"];
            if ([content objectForKey:@"no"] != [NSNull null])
                _noVotes = [content objectForKey:@"no"];
            if ([content objectForKey:@"eventdate"] != [NSNull null])
                _message = [content objectForKey:@"message"];
            if ([content objectForKey:@"subleasername"] != [NSNull null])
                _subName = [jsonDictionary objectForKey:@"subleasername"];
            if ([content objectForKey:@"eventdate"] != [NSNull null])
                _eventdate = [jsonHelp convertDateFromJSON:[content objectForKey:@"eventdate"]];
            if ([content objectForKey:@"eventtime"] != [NSNull null])
                _eventtime = [jsonHelp convertTimeFromJSON:[content objectForKey:@"eventtime"]];
            if ([content objectForKey:@"amount"] != [NSNull null])
                _amount = [content objectForKey:@"amount"];
        }
        _cellHeight = [NSNumber numberWithInt:150];
        _cell = [self createCellFromType:_type];
    }
    return self;
}

- (id)initWithSpacer{
    if(self = [self init]) {
        _type = @"spacer";
        _cellHeight = [NSNumber numberWithInt:5];
        _cell = [self createCellFromType:_type];
    }
    return self;
}

- (id)initWithEmptyCardSet{
    if(self = [self init]) {
        _type = @"empty";
        _cellHeight = [NSNumber numberWithInt:50];
        _cell = [self createCellFromType:_type];
    }
    return self;
}

- (id)initWithHeader{
    if(self = [self init]) {
        _type = @"header";
        _cellHeight = [NSNumber numberWithInt:85];
        _cell = [self createCellFromType:_type];
    }
    return self;
}

- (id)initWithCommentBlockData:(NSDictionary*)data{
    if(self = [self init]) {
        _dict = data;
        _comments = [data objectForKey:@"comments"];
        //NSLog(@"Comments: %@", _comments);
        _cardID = @"0000A1";
        _type = @"commentBlock";
        _cellHeight = [NSNumber numberWithInt:30];
    }
    return self;
}

- (NSObject *)getPropertyByName:(NSString *)prop{
    if ([prop isEqualToString:@"title"])
        return _title;
    else if ([prop isEqualToString:@"type"])
        return _type;
    else if ([prop isEqualToString:@"date"])
        return _date;
    else if ([prop isEqualToString:@"owner"])
        return _owner;
    else if ([prop isEqualToString:@"comments"])
        return _comments;
    else if ([prop isEqualToString:@"cell"])
        return _cell;
    else if ([prop isEqualToString:@"cellHeight"])
        return _cellHeight;
    else if ([prop isEqualToString:@"amount"])
        return _amount;
    else if ([prop isEqualToString:@"cardID"])
        return _cardID;
    else if ([prop isEqualToString:@"dict"])
        return _dict;
    else if ([prop isEqualToString:@"commentButton"])
        return _commentButton;
    else if ([prop isEqualToString:@"likes"])
        return _likes;
    else if ([prop isEqualToString:@"subleasername"])
        return _subName;
    else if ([prop isEqualToString:@"message"])
        return _message;
    else if ([prop isEqualToString:@"eventdate"])
        return _eventdate;
    else if ([prop isEqualToString:@"yesvotes"])
        return _yesVotes;
    else if ([prop isEqualToString:@"novotes"])
        return _noVotes;
    else if ([prop isEqualToString:@"eventtime"])
        return _eventtime;
    else if ([prop isEqualToString:@"house"])
        return _house;

    return [[NSObject alloc] init];
}

- (UITableViewCell *)createCellFromType:(NSString*)type{
    if ([type isEqualToString:@"payment"]){
        //owner, amount, date, cellheight
        _cellHeight = [NSNumber numberWithInteger:85];
        NSArray *info = [NSArray arrayWithObjects:_owner,_amount,_date,_cellHeight,_title, nil];
        //NSLog(@"Info: %@", info);
        ActionCardCell *cell = [[ActionCardCell alloc] initWithPaymentCard:info];
        cell.dict = _dict;
        cell.cellID = _cardID;
        return cell;
    }
    else if ([type isEqualToString:@"setup"]){
        //title, date, cellheight
        _cellHeight = [NSNumber numberWithInteger:85];
        NSArray *info = [NSArray arrayWithObjects:_title,_date,_cellHeight, nil];
        ActionCardCell *cell = [[ActionCardCell alloc] initWithSetupCard:info];
        cell.dict = _dict;
        cell.cellID = _cardID;
        return cell;
    }
    else if ([type isEqualToString:@"announcement"]){
        //title, height, postedBy, date, likes
        NSArray *info = [NSArray arrayWithObjects:_title,_cellHeight,_owner,_date, _likes, nil];
        BasicCardCell *cell = [[BasicCardCell alloc] initWithAnnouncement:info];
        _cellHeight = [cell getCellHeight];
        cell.dict = _dict;
        cell.cellID = _cardID;
        return cell;
    }
    else if ([type isEqualToString:@"vote"]){
        //title, height, date, yesvotes, novotes
        _cellHeight = [NSNumber numberWithInteger:100];
        NSArray *info = [NSArray arrayWithObjects:_title,_cellHeight,_date, _yesVotes, _noVotes,_owner, nil];
        VoteCardCell *cell = [[VoteCardCell alloc] initWithVote:info];
        cell.dict = _dict;
        cell.cellID = _cardID;
        return cell;
    }
    else if ([type isEqualToString:@"task"]){
        //title, height, owner, date
        NSArray *info = [NSArray arrayWithObjects:_title,_cellHeight,_owner, _date, nil];
        BasicCardCell *cell = [[BasicCardCell alloc] initWithTask:info];
        _cellHeight = [cell getCellHeight];
        cell.dict = _dict;
        cell.cellID = _cardID;
        return cell;
    }
    else if ([type isEqualToString:@"event"]){
        //title, height, owner, date, message
        _cellHeight = [NSNumber numberWithInteger:93];
        NSArray *info = [NSArray arrayWithObjects:_title,_cellHeight,_owner, _eventdate, _message, nil];
        //NSLog(@"Info: %@", info);
        EventCardCell *cell = [[EventCardCell alloc] initWithEvent:info];
        cell.dict = _dict;
        cell.cellID = _cardID;
        return cell;
    }
    else if ([type isEqualToString:@"sublease request"]){
        //0.title(NSString), 1.date(NSDate), 2.message(NSString), 3.subleaserName(NSString),  4.height(NSNumber)
        _cellHeight = [NSNumber numberWithInteger:85];
        NSArray *info = [NSArray arrayWithObjects:_title,_date, _message,_subName, _cellHeight, nil];
        ActionCardCell *cell = [[ActionCardCell alloc] initWithSubleaseRequestCard:info];
        cell.dict = _dict;
        cell.cellID = _cardID;
        return cell;
    }
    else if ([type isEqualToString:@"spacer"]){
        return [[CellSpacer alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"spacer"];
    }
    else if ([type isEqualToString:@"empty"]){
        return [[EmptySetCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"empty"];
    }
    else if ([type isEqualToString:@"header"]){
        HeaderCardCell *cell = [[HeaderCardCell alloc] initWithHeight:_cellHeight];
        //_cellHeight = [cell cellHeight:_title];
        [cell setDict:_dict];
        [cell setCellID:_cardID];
        return cell;
    }
    
    return [[UITableViewCell alloc] init];
}

#pragma mark - Card Creation Stuff
+ (void)createCardWithType:(CardType)t withInfo:(NSDictionary*)info{
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser password:baseFrontdoorCredPass persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred andURL:CARD_CREATE_URL andDelegate:self];
    NSArray *params;
    switch (t) {
        case kCardVote:
            params = @[ @{ @"name": @"type", @"value": @"vote" },
                        info ];
            break;
        case kCardAnnouncement:
            params = @[ @{ @"name": @"type", @"value": @"announcement" },
                     info ];
            break;
        case kCardSubleaseRequest:
            break;
        default:
            break;
    }
    [bc postWithParams:params];
}

+ (void)createCardWithType:(CardType)t withArray:(NSArray*)info{
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser password:baseFrontdoorCredPass persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred andURL:CARD_CREATE_URL andDelegate:self];
    NSArray *params;
    switch (t) {
        case kCardTask:{
            params = @[ @{ @"name": @"type", @"value": @"task" }, [info objectAtIndex:0], [info objectAtIndex:1]];
        }
            break;
        case kCardEvent:
            break;
        case kCardPayment:
            params = @[ @{ @"name": @"type", @"value": @"payment" }, [info objectAtIndex:0], [info objectAtIndex:1], [info objectAtIndex:2]];
            break;
        case kCardSubleaseRequest:
            break;
        default:
            break;
    }
    [bc postWithParams:params];
}



@end
