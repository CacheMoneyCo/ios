//
//  Constants.m
//  
//
//  Created by Nathan Amarandos on 5/5/18.
//

#import "Constants.h"

NSString *const baseFrontdoorURL = @"http://127.0.0.1:8000/api/";
NSString *const baseFrontdoorCredUser = @"testsignup";
NSString *const baseFrontdoorCredPass = @"strongpassword";
