//
//  NewHouse.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/3/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewHouse : NSObject
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSNumber *numBeds;
@property (nonatomic, retain) NSNumber *numBaths;
@property (nonatomic, retain) NSNumber *rentAmount;
+ (id)initWithAddress:(NSString*)addrIn andNumBeds:(NSNumber*)numBedsIn andNumBaths:(NSNumber*)numBathsIn andRentAmount:(NSNumber*)rentAmountIn;

- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)decoder;

@end
