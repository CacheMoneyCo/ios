//
//  Constants.h
//  
//
//  Created by Nathan Amarandos on 5/5/18.
//

#import <Foundation/Foundation.h>

extern NSString *const baseFrontdoorURL;
extern NSString *const baseFrontdoorCredUser;
extern NSString *const baseFrontdoorCredPass;

typedef enum
{
    kCardAnnouncement = 1,
    kCardPayment,
    kCardTask,
    kCardSubleaseRequest,
    kCardSetup,
    kCardVote,
    kCardEvent
} CardType;
