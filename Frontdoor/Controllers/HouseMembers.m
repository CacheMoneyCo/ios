//
//  HouseMembers.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/7/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "HouseMembers.h"
#define GET_MEMBER_URL [NSString stringWithFormat:@"%@lease/users/", baseFrontdoorURL]

@implementation HouseMembers

- (void)getMembers{
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser
                                                         password:baseFrontdoorCredPass
                                                      persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred
                                                                  andURL:GET_MEMBER_URL
                                                             andDelegate:self];
    [bc requestData];
}

+ (BOOL)isUser:(NSString*)username{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSDictionary *member = [def objectForKey:@"member_self"];
    if (member){
        return [[member objectForKey:@"username"] isEqualToString:username];
    }
    return false;
}

#pragma mark - NSURLSessionDelegate

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
   didReceiveData:(NSData *)data {
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    if (dictionary){
        dispatch_async(dispatch_get_main_queue(), ^{
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            [def setObject:dictionary forKey:@"houseMembers"];
            for (NSDictionary* member in dictionary){
                if ([[member objectForKey:@"is_self"] isEqual:[NSNumber numberWithInteger:1]]){
                    [def setObject:member forKey:@"member_self"];
                }
            }
        });
    }
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler{
    if ([challenge previousFailureCount] == 0) {
        NSLog(@"received and handle authentication challenge");
        NSURLCredential *newCredential = [NSURLCredential credentialWithUser:baseFrontdoorCredUser
                                                                    password:baseFrontdoorCredPass
                                                                 persistence:NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
    }
    else {
        NSLog(@"previous authentication failure");
    }
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error{
    NSLog(@"%@", error);
}

@end
