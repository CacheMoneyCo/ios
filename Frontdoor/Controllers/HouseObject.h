//
//  HouseObject.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/6/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HouseObject : NSObject

@property (nonatomic, retain) NSNumber *hID;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSDate *startDate;
@property (nonatomic, retain) NSDate *endDate;
@property (nonatomic, retain) NSNumber *isLandlord;
@property (nonatomic, retain) NSNumber *isCurrent;
@property (nonatomic, retain) NSString *viewId;
@property (nonatomic, retain) NSString *imageName;

+ (id)houseWithProperties:(NSArray*)info;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)decoder;
@end
