//
//  HouseRooms.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/7/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "HouseRooms.h"
#define GET_ROOMS_URL [NSString stringWithFormat:@"%@rooms/", baseFrontdoorURL]

@implementation HouseRooms

- (void)getRooms{
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser
                                                         password:baseFrontdoorCredPass
                                                      persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred
                                                                  andURL:GET_ROOMS_URL
                                                             andDelegate:self];
    [bc requestData];
}

- (void)getRoomsWithWithCompletionHandler:(void (^)(void))completionHandler{
    NSURLCredential *cred = [[NSURLCredential alloc] initWithUser:baseFrontdoorCredUser
                                                         password:baseFrontdoorCredPass
                                                      persistence:NSURLCredentialPersistenceForSession];
    BackendConnector *bc = [[BackendConnector alloc] initWithCredentials:cred
                                                                  andURL:GET_ROOMS_URL
                                                             andDelegate:self];
    [bc requestDataWithCompletionHandler:^(NSData *data){
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            [def setObject:dictionary forKey:@"houseRooms"];
            completionHandler();
        });
    }];
}

#pragma mark - NSURLSessionDelegate

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
   didReceiveData:(NSData *)data {
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    if (dictionary){
        dispatch_async(dispatch_get_main_queue(), ^{
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            [def setObject:dictionary forKey:@"houseRooms"];
        });
    }
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler{
    if ([challenge previousFailureCount] == 0) {
        NSLog(@"received and handle authentication challenge");
        NSURLCredential *newCredential = [NSURLCredential credentialWithUser:baseFrontdoorCredUser
                                                                    password:baseFrontdoorCredPass
                                                                 persistence:NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
    }
    else {
        NSLog(@"previous authentication failure");
    }
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error{
    NSLog(@"%@", error);
}

@end

