//
//  HouseObject.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/6/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "HouseObject.h"

@implementation HouseObject

+ (id)houseWithProperties:(NSArray*)info{
    HouseObject *h = [[HouseObject alloc] init];
    h.hID = [info objectAtIndex:0];
    h.name = [info objectAtIndex:1];
    h.startDate = [info objectAtIndex:2];
    h.endDate = [info objectAtIndex:3];
    h.isLandlord = [info objectAtIndex:4];
    h.isCurrent = [info objectAtIndex:5];
    h.viewId = [info objectAtIndex:6];
    h.imageName = [info objectAtIndex:7];
    return h;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.hID forKey:@"hID"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.startDate forKey:@"startDate"];
    [encoder encodeObject:self.hID forKey:@"endDate"];
    [encoder encodeObject:self.name forKey:@"isLandlord"];
    [encoder encodeObject:self.startDate forKey:@"isCurrent"];
    [encoder encodeObject:self.hID forKey:@"viewId"];
    [encoder encodeObject:self.name forKey:@"imageName"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.hID = [decoder decodeObjectForKey:@"hID"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.startDate = [decoder decodeObjectForKey:@"startDate"];
        self.endDate = [decoder decodeObjectForKey:@"endDate"];
        self.isLandlord = [decoder decodeObjectForKey:@"isLandlord"];
        self.isCurrent = [decoder decodeObjectForKey:@"isCurrent"];
        self.viewId = [decoder decodeObjectForKey:@"viewId"];
        self.imageName = [decoder decodeObjectForKey:@"imamgeName"];

    }
    return self;
}

@end
