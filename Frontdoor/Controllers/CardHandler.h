//
//  CardHandler.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/14/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


#import "CellSpacer.h"
#import "BasicCardCell.h"
#import "ActionCardCell.h"
#import "HeaderCardCell.h"
#import "CommentBlockCell.h"
#import "EmptySetCell.h"
#import "VoteCardCell.h"
#import "EventCardCell.h"
#import "JSONParser.h"
#import "CommentButton.h"

#import "BackendConnector.h"

#import "FeedViewController.h"

@class JSONParser;

@interface CardHandler : NSObject
{
    JSONParser *jsonHelp;
}

- (id)initWithJSONDictionary:(NSDictionary *)jsonDictionary;
- (NSObject *)getPropertyByName:(NSString *)prop;
- (id)initWithSpacer;
- (id)initWithEmptyCardSet;
- (id)initWithHeader;
- (id)initWithCommentBlockData:(NSDictionary*)data;
+ (void)createCardWithType:(CardType)t withInfo:(NSDictionary*)info;
+ (void)createCardWithType:(CardType)t withArray:(NSArray*)info;

@property (readonly) NSString   *type;
@property (readonly) NSDate     *date;
@property (readonly) NSDate     *eventdate;
@property (readonly) NSDate     *eventtime;
@property (readonly) NSString   *title;
@property (readonly) NSString   *house;
@property (readonly) NSString   *message;
@property (readonly) NSString   *owner;
@property (readonly) NSArray    *comments;
@property (readonly) NSArray    *yesVotes;
@property (readonly) NSArray    *noVotes;
@property (readonly) NSArray    *likes;
@property (readonly) NSDictionary *dict;
@property (readonly) UITableViewCell *cell;
@property (readonly) NSNumber *cellHeight;
@property (readonly) NSNumber *amount;
@property (readonly) NSString *cardID;
@property (readonly) NSString *subName;
@property (strong, nonatomic) CommentButton *commentButton;//get rid of

@end
