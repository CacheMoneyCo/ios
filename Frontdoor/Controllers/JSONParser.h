//
//  JSONParser.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/2/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CardHandler.h"
#import "HouseObject.h"
#import "ChatObject.h"

@interface JSONParser : NSObject
{
    NSString *uid;
}
@property (nonatomic, retain)NSError *fetchError;
- (NSDate*)convertDateFromJSON:(NSString*)str;
- (NSDate*)convertTimeFromJSON:(NSString*)str;
- (NSDate*)convertDateAndTimeFromJSON:(NSString*)str;
- (NSMutableArray*)parseCardDict:(NSDictionary*)dict;
- (NSMutableArray*)parseHouseDict:(NSDictionary*)dict;
- (NSMutableArray*)parseChatDict:(NSDictionary*)dict;
- (HouseObject*)getHouseInfoWithID:(NSNumber*)hID fromDict:(NSDictionary*)dict;
@end
