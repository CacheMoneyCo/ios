//
//  ExtrasCellObject.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/15/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ExtrasCellObject : NSObject
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) NSString *identifier;
+ (id)initWithTitle:(NSString*)titleIn andImageNamed:(NSString*)imageIn andIdentifier:(NSString*)identifierIn;

- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)decoder;

@end
