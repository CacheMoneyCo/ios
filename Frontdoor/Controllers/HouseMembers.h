//
//  HouseMembers.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/7/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "JSONParser.h"
#import "BackendConnector.h"

@class JSONParser;

@interface HouseMembers : NSObject <NSURLSessionDelegate>
{
    JSONParser *jp;
}
- (void)getMembers;
+ (BOOL)isUser:(NSString*)username;
@end
