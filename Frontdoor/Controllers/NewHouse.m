//
//  NewHouse.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/3/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "NewHouse.h"

@implementation NewHouse
+ (id)initWithAddress:(NSString*)addrIn andNumBeds:(NSNumber*)numBedsIn andNumBaths:(NSNumber*)numBathsIn andRentAmount:(NSNumber*)rentAmountIn{
    NewHouse *n = [[NewHouse alloc] init];
    n.address = addrIn;
    n.numBeds = numBedsIn;
    n.numBaths = numBathsIn;
    n.rentAmount = rentAmountIn;
    return n;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.address forKey:@"addr"];
    [encoder encodeObject:self.numBeds forKey:@"numBeds"];
    [encoder encodeObject:self.numBaths forKey:@"numBaths"];
    [encoder encodeObject:self.rentAmount forKey:@"rentAmount"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.address = [decoder decodeObjectForKey:@"addr"];
        self.numBeds = [decoder decodeObjectForKey:@"numBeds"];
        self.numBaths = [decoder decodeObjectForKey:@"numBaths"];
        self.rentAmount = [decoder decodeObjectForKey:@"rentAmount"];
    }
    return self;
}
@end
