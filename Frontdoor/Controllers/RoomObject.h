//
//  RoomObject.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/17/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoomObject : NSObject
@property (nonatomic, retain)NSString *name;
@property (nonatomic, retain)NSNumber *sqFootage;
@property (nonatomic, retain)NSNumber *rentAmount;
@property (nonatomic, retain)NSNumber *numInhabitant;
@property (nonatomic, retain)NSNumber *hasBath;
@property (nonatomic, retain)NSNumber *hasAwkLayout;
@property (nonatomic, retain)NSNumber *hasCloset;
@property (nonatomic, retain)NSNumber *rent;
+ (RoomObject*)initWithName:(NSString*)nm sqFootage:(NSNumber*)sq numInhabitants:(NSNumber*)ni bath:(NSNumber*)b awk:(NSNumber*)a closet:(NSNumber*)c rent:(NSNumber*)r;
@end
