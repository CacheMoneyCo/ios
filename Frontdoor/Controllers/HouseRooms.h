//
//  HouseRooms.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 5/8/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "JSONParser.h"
#import "BackendConnector.h"

@class JSONParser;

@interface HouseRooms : NSObject <NSURLSessionDelegate>
{
    JSONParser *jp;
}
- (void)getRooms;
- (void)getRoomsWithWithCompletionHandler:(void (^)(void))completionHandler;
@end

