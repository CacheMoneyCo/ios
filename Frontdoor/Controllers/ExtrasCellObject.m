//
//  ExtrasCellObject.m
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/15/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import "ExtrasCellObject.h"

@implementation ExtrasCellObject
@synthesize title, image, identifier;
+ (id)initWithTitle:(NSString*)titleIn andImageNamed:(NSString*)imageIn andIdentifier:(NSString*)identifierIn{
    ExtrasCellObject *cell = [[ExtrasCellObject alloc] init];
    cell.image = [UIImage imageNamed:imageIn];
    cell.title = titleIn;
    cell.identifier = identifierIn;
    return cell;
}

- (void)make{
    
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.title forKey:@"title"];
    [encoder encodeObject:self.image forKey:@"image"];
    [encoder encodeObject:self.identifier forKey:@"identifier"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.title = [decoder decodeObjectForKey:@"title"];
        self.image = [decoder decodeObjectForKey:@"image"];
        self.identifier = [decoder decodeObjectForKey:@"identifier"];
    }
    return self;
}
@end
