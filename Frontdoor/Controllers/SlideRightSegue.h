//
//  SlideRightSegue.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 3/31/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideRightSegue : UIStoryboardSegue

- (void) performSegueFrom:(UIViewController*)src to:(UIViewController*)dst;

@end
