//
//  BackendConnector.h
//  Frontdoor
//
//  Created by Nathan Amarandos on 4/30/18.
//  Copyright © 2018 Cache Money. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "Constants.h"
#import "HouseRooms.h"

@interface BackendConnector : NSObject <NSURLSessionDelegate>
{
    NSURLCredential *credentials;
    NSURL *URL;
    id delegate;
}

- (id)initWithCredentials:(NSURLCredential*)creds andURL:(NSString*)u andDelegate:(id)del;
- (void)requestData;
- (void)postWithParams:(NSArray*)params;
- (void)postWithParamsAndRoomRefresh:(NSArray*)params completionHandler:(void (^)(void))completionHandler;
- (void)requestDataWithCompletionHandler:(void (^)(NSData* data))completionHandler;
@end
